<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserProfession */

$this->title = Yii::t('backend', 'Create User Profession');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'User Professions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profession-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
