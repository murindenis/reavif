<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getPublicIdentity();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'auth_key',
            'email:email',
            [
                'attribute' => 'status',
                'value' => function (\common\models\User $model) {
                    switch ($model->status) {
                        case 1:
                            return 'Not Active';
                            break;
                        case 2:
                            return 'Active';
                            break;
                        case 3:
                            return 'Deleted';
                            break;
                    }
                }
            ],
            //'status',
            'created_at:datetime',
            'updated_at:datetime',
            'logged_at:datetime',
        ],
    ]) ?>

    <h3>Profile</h3>
    <?php echo DetailView::widget([
        'model' => $modelProfile,
        'attributes' => [
            //'id',
            'firstname',
            'middlename',
            'lastname',
            'phone',
            'country',
            'city',
            'street',
            'house',
            'apartment'
        ],
    ]) ?>

</div>
