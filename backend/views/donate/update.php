<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Donate */

$this->title = Yii::t('backend', 'Update');
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="donate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
