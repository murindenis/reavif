<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Donate */

$this->title = Yii::t('backend', 'Create Donate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Donates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
