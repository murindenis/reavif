<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Donate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_eng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'title_esp')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'make_donation_eng')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'make_donation_esp')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
