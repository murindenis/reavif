<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ShopOrders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Shop Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-orders-view">

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'label' => 'Order ID',
            ],
            'user_id',
            'created_at:datetime',
        ],
    ]) ?>

    <h3>Profile</h3>
    <?php echo DetailView::widget([
        'model' => $modelProfile,
        'attributes' => [
            [
                'label' => 'Email',
                'value' => function (\common\models\UserProfile $model) {
                    return $model->user->email;
                }
            ],
            'firstname',
            'middlename',
            'lastname',
            'phone',
            'country',
            'city',
            'street',
            'house',
            'apartment'
        ],
    ]) ?>

    <div id="orderProductTarget">
        <div id="orderProductContainer">
            <h3>Order Products</h3>
            <?= \yii\grid\GridView::widget([
                'id' => 'orderProductView',
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'label' => 'Photo',
                        'format' => 'raw',
                        'value' => function ($shopOrdersProduct) {
                            return '<img src="'.\common\behaviors\GlobalHelper::getImgPath($shopOrdersProduct->shopProduct).'" style="height:120px;">';
                        },
                    ],
                    [
                        'label' => 'Name_eng',
                        'value' => function ($shopOrdersProduct) {
                            return $shopOrdersProduct->shopProduct->name_eng;
                        }
                    ],
                    [
                        'label' => 'Price',
                        'value' => function ($shopOrdersProduct) {
                            return $shopOrdersProduct->shopProduct->price;
                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{show} {delete}',

                        'buttons' => [
                            'show' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['shop-product/view', 'id' => $model->shopProduct->id]);
                            },

                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['shop-orders-product/delete', 'id' => $model->id, 'shopOrdersId' => Yii::$app->request->get('id')], [
                                    'data' => [
                                        'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

            <!--<div id="orderProductTotal" style="font-weight: 600; font-size: 18px;">
                Total Price:
            </div>-->
        </div>
    </div>

</div>
