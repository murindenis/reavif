<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShopOrders */

$this->title = Yii::t('backend', 'Create Shop Orders');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Shop Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-orders-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
