<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ShopOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Shop Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-orders-index">

    <p>
        <?= Html::a(Yii::t('backend', 'Create Shop Orders'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function (\common\models\ShopOrders $data) {
                    //d($data);die;
                    return Html::a($data->user_id, ['/user/view', 'id' => $data->user_id], ['target' => '_blank']);
                }
            ],
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
