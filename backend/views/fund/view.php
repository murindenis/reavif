<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Fund */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Funds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fund-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fund_eng:ntext',
            'fund_esp:ntext',
            'problem_eng:ntext',
            'problem_esp:ntext',
            'mission_eng:ntext',
            'mission_esp:ntext',
            'objectives_eng:ntext',
            'objectives_esp:ntext',
            'partners_eng:ntext',
            'partners_esp:ntext',
        ],
    ]) ?>

</div>
