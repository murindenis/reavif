<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Fund */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fund-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">

        <?= $form->field($model, 'fund_eng')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>
        <?= $form->field($model, 'fund_esp')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>

        <?= $form->field($model, 'problem_eng')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>
        <?= $form->field($model, 'problem_esp')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>

        <?= $form->field($model, 'mission_eng')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>
        <?= $form->field($model, 'mission_esp')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>

        <?= $form->field($model, 'objectives_eng')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>
        <?= $form->field($model, 'objectives_esp')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>

        <?= $form->field($model, 'partners_eng')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>
        <?= $form->field($model, 'partners_esp')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                'options'=> [
                    'minHeight'   => 300,
                    'maxHeight'   => 600,
                    'buttonSource'=> true,
                    'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                ]
            ]
        ) ?>

        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
