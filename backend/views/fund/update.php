<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Fund */

$this->title = Yii::t('backend', 'Update About Fund');
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="fund-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
