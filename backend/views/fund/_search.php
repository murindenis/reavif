<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\FundSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fund-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fund_eng') ?>

    <?= $form->field($model, 'fund_esp') ?>

    <?= $form->field($model, 'problem_eng') ?>

    <?= $form->field($model, 'problem_esp') ?>

    <?php // echo $form->field($model, 'mission_eng') ?>

    <?php // echo $form->field($model, 'mission_esp') ?>

    <?php // echo $form->field($model, 'objectives_eng') ?>

    <?php // echo $form->field($model, 'objectives_esp') ?>

    <?php // echo $form->field($model, 'partners_eng') ?>

    <?php // echo $form->field($model, 'partners_esp') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
