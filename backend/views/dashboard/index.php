<?php
/**
 * Author: Eugine Terentev <eugine@terentev.net>
 * @var $this \yii\web\View
 * @var $provider \probe\provider\ProviderInterface
 */
use common\models\FileStorageItem;
use common\models\User;
use common\models\Material;
use common\models\Hashtag;

$this->title = Yii::t('backend', 'Dashboard');
$this->registerJs("window.paceOptions = { ajax: false }", \yii\web\View::POS_HEAD);
$this->registerJsFile(
    Yii::$app->request->baseUrl . 'js/system-information/index.js',
    ['depends' => ['\yii\web\JqueryAsset', '\common\assets\Flot', '\yii\bootstrap\BootstrapPluginAsset']]
) ?>
<div id="dashboard-index">

    <div class="row">
        <!-- USERS -->
        <div class="col-md-12"><h1>Users</h1></div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner">
                    <p>
                        <?php echo Yii::t('backend', 'User Registrations') ?>
                    </p>
                    <h3>
                        <?php echo User::find()->count() ?>
                    </h3>
                </div>
                <div class="icon">
                    <i class="glyphicon glyphicon-user"></i>
                </div>
                <a href="<?php echo Yii::$app->urlManager->createUrl(['/user/index']) ?>" class="small-box-footer">
                    <?php echo Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner">
                    <p>
                        <?php echo Yii::t('backend', 'Subscribes') ?>
                    </p>
                    <h3>
                        <?php echo User::find()->count() ?>
                    </h3>
                </div>
                <div class="icon">
                    <i class="glyphicon glyphicon-envelope"></i>
                </div>
                <a href="<?php echo Yii::$app->urlManager->createUrl(['/subscribe/index']) ?>" class="small-box-footer">
                    <?php echo Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- MATERIALS -->
        <div class="col-md-12"><h1>Materials</h1></div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <p>
                        <?php echo Yii::t('backend', 'Hashtags') ?>
                    </p>
                    <h3>
                        <?php echo Hashtag::find()->count() ?>
                    </h3>
                </div>
                <div class="icon">
                    <i class="glyphicon glyphicon-tag"></i>
                </div>
                <a href="<?php echo Yii::$app->urlManager->createUrl(['/hashtag/index']) ?>" class="small-box-footer">
                    <?php echo Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <p>
                        <?php echo Yii::t('backend', 'Materials') ?>
                    </p>
                    <h3>
                        <?php echo Material::find()->count() ?>
                    </h3>
                </div>
                <div class="icon">
                    <i class="glyphicon glyphicon-file"></i>
                </div>
                <a href="<?php echo Yii::$app->urlManager->createUrl(['/material/index']) ?>" class="small-box-footer">
                    <?php echo Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- SHOP -->
        <div class="col-md-12"><h1>Shop</h1></div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <p>
                        <?php echo Yii::t('backend', 'Orders') ?>
                    </p>
                    <h3>
                        <?php echo \common\models\ShopOrders::find()->count() ?>
                    </h3>
                </div>
                <div class="icon">
                    <i class="glyphicon glyphicon-shopping-cart"></i>
                </div>
                <a href="<?php echo Yii::$app->urlManager->createUrl(['/shop-order/index']) ?>" class="small-box-footer">
                    <?php echo Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <p>
                        <?php echo Yii::t('backend', 'Products') ?>
                    </p>
                    <h3>
                        <?php echo \common\models\ShopProduct::find()->count() ?>
                    </h3>
                </div>
                <div class="icon">
                    <i class="glyphicon glyphicon-list-alt"></i>
                </div>
                <a href="<?php echo Yii::$app->urlManager->createUrl(['/shop-product/index']) ?>" class="small-box-footer">
                    <?php echo Yii::t('backend', 'More info') ?> <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
