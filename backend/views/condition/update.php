<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Condition */

$this->title = Yii::t('backend', 'Update Condition');
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="condition-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
