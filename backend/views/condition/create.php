<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Condition */

$this->title = Yii::t('backend', 'Create Condition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Conditions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="condition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
