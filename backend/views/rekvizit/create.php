<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Rekvizit */

$this->title = Yii::t('backend', 'Create Requisites');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Requisites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekvizit-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
