<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Rekvizit */

$this->title = Yii::t('backend', 'Update Requisites');
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="rekvizit-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
