<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Rekvizit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekvizit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address_eng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_esp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ccc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iban_electronic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iban_paper')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
