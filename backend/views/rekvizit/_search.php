<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\RekvizitSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekvizit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'address_eng') ?>

    <?= $form->field($model, 'address_esp') ?>

    <?= $form->field($model, 'ccc') ?>

    <?= $form->field($model, 'iban_electronic') ?>

    <?php // echo $form->field($model, 'iban_paper') ?>

    <?php // echo $form->field($model, 'bic') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
