<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Problem */

$this->title = 'Create Relevance';
$this->params['breadcrumbs'][] = ['label' => 'Relevances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="problem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
