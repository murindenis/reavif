<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Problem */

$this->title = 'Update Relevance: ' . $model->id;
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="problem-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
