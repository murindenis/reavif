<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShopOrdersProduct */

$this->title = Yii::t('backend', 'Create Shop Orders Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Shop Orders Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-order-product-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
