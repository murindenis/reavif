<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShopOrdersProduct */

$this->title = Yii::t('backend', 'Update Shop Orders Product: {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Shop Orders Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="shop-order-product-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
