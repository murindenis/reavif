<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShopOrdersProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-order-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shop_orders_id')->textInput() ?>

    <?= $form->field($model, 'shop_product_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
