<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Social */

$this->title = 'Update Social network: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Social networks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="social-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
