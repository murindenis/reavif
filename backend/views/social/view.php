<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Social */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Social network', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'link',
        ],
    ]) ?>

</div>
