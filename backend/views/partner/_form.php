<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Partner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'site_link')->textInput(['maxlength' => true])?>

    <?php echo $form->field($model, 'picture')->widget(
        \trntv\filekit\widget\Upload::classname(),
        [
            'url' => ['avatar-upload']
        ]
    )->label('Картинка') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
