<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Partner */

$this->title = Yii::t('backend', 'Create Partner');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Partners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
