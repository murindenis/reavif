<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShopProduct */

$this->title = Yii::t('backend', 'Create Shop Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Shop Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-product-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
