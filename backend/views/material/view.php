<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Material */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-view">

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Hashtags',
                'value' => function($model){
                    if ($model->materialHashtag) {
                        $hashtagArray = [];
                        foreach ($model->materialHashtag as $hashtag) {
                            $hashtagArray[] = $hashtag->hashtag->name_eng;
                        }
                        return implode(", " , $hashtagArray);
                    } else {
                        return 'No Hashtag';
                    }

                }
            ],
            'category.name_eng',
            'user_id',
            'title_eng',
            'title_esp',
            'slug',
            'hashtag.name_eng',
            'introtext_eng',
            'introtext_esp',
            'description_eng:ntext',
            'description_esp:ntext',
            'status:boolean',
            'created_at:datetime',
        ],
    ]) ?>

</div>
