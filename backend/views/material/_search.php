<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\MaterialSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'title_eng') ?>

    <?= $form->field($model, 'title_esp') ?>

    <?= $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'hashtag_id') ?>

    <?php // echo $form->field($model, 'introtext_eng') ?>

    <?php // echo $form->field($model, 'introtext_esp') ?>

    <?php // echo $form->field($model, 'description_eng') ?>

    <?php // echo $form->field($model, 'description_esp') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
