<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\Material */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="material-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList($model->getCategoryColumn(), ['prompt' => '-']) ?>

    <?= $form->field($model, 'hashtags')->widget(\dosamigos\multiselect\MultiSelect::className(),[
        'data' => $model->getHashtagColumn(),
        "options" => ['multiple'=>"multiple"],
        "clientOptions" =>
            [
                'enableFiltering' => true,
            ],
    ]) ?>

    <?= $form->field($model, 'title_eng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_esp')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hashtag_id')->dropDownList($model->getHashtagColumn(), ['prompt' => '-']) ?>

    <?= $form->field($model, 'introtext_eng')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'introtext_esp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_eng')->widget(
        \yii\imperavi\Widget::className(),
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
            'options'=> [
                'minHeight'   => 300,
                'maxHeight'   => 600,
                'buttonSource'=> true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ) ?>

    <?= $form->field($model, 'description_esp')->widget(
        \yii\imperavi\Widget::className(),
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
            'options'=> [
                'minHeight'   => 300,
                'maxHeight'   => 600,
                'buttonSource'=> true,
                'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ) ?>

    <div class="event_at">
        <label class="control-label" for="material-event_at">Event Date</label>
        <?= \janisto\timepicker\TimePicker::widget([
            //'language' => 'fi',
            'model' => $model,
            'attribute' => 'event_at',
            'mode' => 'datetime',
            'clientOptions' => [
                'dateFormat' => 'yy-mm-dd',
                'timeFormat' => 'HH:mm:ss',
                'showSecond' => true,
            ]
        ]);
        ?>
        <?= $form->field($model, 'place_eng')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'place_esp')->textInput(['maxlength' => true]) ?>
    </div>

    <?php echo $form->field($model, 'picture')->widget(
        \trntv\filekit\widget\Upload::classname(),
        [
            'url' => ['avatar-upload']
        ]
    ) ?>

    <label class="control-label" for="material-event_at">Created</label>
    <?= \janisto\timepicker\TimePicker::widget([
        //'language' => 'fi',
        'model' => $model,
        'attribute' => 'created_at',
        'mode' => 'datetime',
        'clientOptions' => [
            'dateFormat' => 'yy-mm-dd',
            'timeFormat' => 'HH:mm:ss',
            'showSecond' => true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
