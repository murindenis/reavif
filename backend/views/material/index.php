<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Materials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Material'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'category_id',
                'value'     => 'category.name_eng',
                'filter' => Html::activeDropDownList($searchModel, 'category_id', \yii\helpers\ArrayHelper::map(\common\models\Category::find()->all(), 'id', 'name_eng'), ['prompt' => '-', 'class' => 'form-control']),

            ],
            'title_eng',
            'title_esp',
            'slug',
            'status:boolean',
            'user_id',
            //'introtext_eng',
            //'introtext_esp',
            //'description_eng:ntext',
            //'description_esp:ntext',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
