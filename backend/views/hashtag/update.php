<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Hashtag */

$this->title = Yii::t('backend', 'Update Hashtag: {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Hashtags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="hashtag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
