<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hashtag */

$this->title = Yii::t('backend', 'Create Hashtag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Hashtags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hashtag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
