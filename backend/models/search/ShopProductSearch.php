<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ShopProduct;

/**
 * ShopProductSearch represents the model behind the search form of `common\models\ShopProduct`.
 */
class ShopProductSearch extends ShopProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price'], 'integer'],
            [['name_eng', 'name_esp', 'description_eng', 'description_esp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name_eng', $this->name_eng])
            ->andFilterWhere(['like', 'name_esp', $this->name_esp])
            ->andFilterWhere(['like', 'description_eng', $this->description_eng])
            ->andFilterWhere(['like', 'description_esp', $this->description_esp]);

        return $dataProvider;
    }
}
