<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Rekvizit;

/**
 * RekvizitSearch represents the model behind the search form of `common\models\Rekvizit`.
 */
class RekvizitSearch extends Rekvizit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['address_eng', 'address_esp', 'ccc', 'iban_electronic', 'iban_paper', 'bic', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rekvizit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'address_eng', $this->address_eng])
            ->andFilterWhere(['like', 'address_esp', $this->address_esp])
            ->andFilterWhere(['like', 'ccc', $this->ccc])
            ->andFilterWhere(['like', 'iban_electronic', $this->iban_electronic])
            ->andFilterWhere(['like', 'iban_paper', $this->iban_paper])
            ->andFilterWhere(['like', 'bic', $this->bic])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
