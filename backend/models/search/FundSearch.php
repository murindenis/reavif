<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Fund;

/**
 * FundSearch represents the model behind the search form of `common\models\Fund`.
 */
class FundSearch extends Fund
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fund_eng', 'fund_esp', 'problem_eng', 'problem_esp', 'mission_eng', 'mission_esp', 'objectives_eng', 'objectives_esp', 'partners_eng', 'partners_esp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fund::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'fund_eng', $this->fund_eng])
            ->andFilterWhere(['like', 'fund_esp', $this->fund_esp])
            ->andFilterWhere(['like', 'problem_eng', $this->problem_eng])
            ->andFilterWhere(['like', 'problem_esp', $this->problem_esp])
            ->andFilterWhere(['like', 'mission_eng', $this->mission_eng])
            ->andFilterWhere(['like', 'mission_esp', $this->mission_esp])
            ->andFilterWhere(['like', 'objectives_eng', $this->objectives_eng])
            ->andFilterWhere(['like', 'objectives_esp', $this->objectives_esp])
            ->andFilterWhere(['like', 'partners_eng', $this->partners_eng])
            ->andFilterWhere(['like', 'partners_esp', $this->partners_esp]);

        return $dataProvider;
    }
}
