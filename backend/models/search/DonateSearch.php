<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Donate;

/**
 * DonateSearch represents the model behind the search form of `common\models\Donate`.
 */
class DonateSearch extends Donate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title_eng', 'title_esp', 'make_donation_eng', 'make_donation_esp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Donate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title_eng', $this->title_eng])
            ->andFilterWhere(['like', 'title_esp', $this->title_esp])
            ->andFilterWhere(['like', 'make_donation_eng', $this->make_donation_eng])
            ->andFilterWhere(['like', 'make_donation_esp', $this->make_donation_esp]);

        return $dataProvider;
    }
}
