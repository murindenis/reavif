<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ShopOrdersProduct;

/**
 * ShopOrdersProductSearch represents the model behind the search form of `common\models\ShopOrdersProduct`.
 */
class ShopOrdersProductSearch extends ShopOrdersProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'shop_orders_id', 'shop_product_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopOrdersProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'shop_orders_id' => $this->shop_orders_id,
            'shop_product_id' => $this->shop_product_id,
        ]);

        return $dataProvider;
    }
}
