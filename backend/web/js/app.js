$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
});

var eventAt = $('#material-category_id');
if (eventAt.val() == 3) {
    $('.event_at').css('display', 'block');
}
eventAt.on('change', function () {
   if($(this).val() == 3) {
       $('.event_at').css('display', 'block');
   } else {
       $('.event_at').css('display', 'none');
   }
});