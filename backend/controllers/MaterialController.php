<?php

namespace backend\controllers;

use common\models\FileStorageItem;
use common\models\MaterialHashtag;
use common\models\MaterialPhoto;
use Yii;
use common\models\Material;
use backend\models\search\MaterialSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\UploadAction;
use yii\rest\DeleteAction;

/**
 * MaterialController implements the CRUD actions for Material model.
 */
class MaterialController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read());
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Material models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Material model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Material model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Material();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            if ($model->event_at) {
                $model->event_at = strtotime($model->event_at);
            }
            if ($model->created_at) {
                $model->created_at = strtotime($model->created_at);
            }
            $model->save();
            $hashtagArray = Yii::$app->request->post()['Material']['hashtags'];
            if (!empty($hashtagArray)) {
                foreach ($hashtagArray as $hashtag) {
                    $newMaterialHashtag = new MaterialHashtag();
                    $newMaterialHashtag->material_id = $model->id;
                    $newMaterialHashtag->hashtag_id  = $hashtag;
                    $newMaterialHashtag->save();
                }
            }
            $this->savePhoto($model);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Material model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $hashtags = MaterialHashtag::find()->select('hashtag_id')->where(['material_id' => $model->id])->column();
        $model->hashtags = $hashtags;
        if ($model->event_at) {
            $model->event_at = date('Y-m-d H:i:s', $model->event_at);
        }
        if ($model->created_at) {
            $model->created_at = date('Y-m-d H:i:s', $model->created_at);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->event_at) {
                $model->event_at = strtotime($model->event_at);
            }
            if ($model->created_at) {
                $model->created_at = strtotime($model->created_at);
            }
            $model->save();
            $materialHashtag = MaterialHashtag::find()->where(['material_id' => $model->id])->all();
            foreach ($materialHashtag as $item) {
                $item->delete();
            }
            $hashtagArray = Yii::$app->request->post()['Material']['hashtags'];
            if (!empty($hashtagArray)) {
                foreach ($hashtagArray as $hashtag) {
                    $newMaterialHashtag = new MaterialHashtag();
                    $newMaterialHashtag->material_id = $model->id;
                    $newMaterialHashtag->hashtag_id  = $hashtag;
                    $newMaterialHashtag->save();
                }
            }

            $this->savePhoto($model);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Material model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Material model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Material the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Material::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('backend', 'The requested page does not exist.'));
    }

    public function savePhoto($material)
    {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/material/' . $material->id;
        if (!empty($material->path)) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }

                $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $material->path;
                if (file_exists($oldPathPhoto)) {
                    $photoPath = explode("/", $material->path);
                    //Копируем фотографии из временной папки 1 в папку с id квартиры
                    if (($photoPath['0'] == '1') || ($photoPath['0'] == $material->id)) {
                        $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                        //Изменяем записи в БД об изображениии
                        if ($copyStatus) {
                            $oldPath = $material->path;
                            $material->updateAttributes(['path' => 'material/' . $material->id . '/' . $photoPath['1']]);
                            //$material->path = 'material/' . $material->id . '/' . $photoPath['1'];
                            $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                            $fileStorageItem->path = 'material/' . $material->id . '/' . $photoPath['1'];
                            $fileStorageItem->save();
                        }
                    }
                }

        }
    }
}
