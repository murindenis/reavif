<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],

        // Articles
        ['pattern' => 'article/index', 'route' => 'article/index'],
        ['pattern' => 'article/attachment-download', 'route' => 'article/attachment-download'],
        ['pattern' => 'article/<slug>', 'route' => 'article/view'],

        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']],

        ['pattern' => '/', 'route' => 'site/index'],
        ['pattern' => 'privacy-policy', 'route' => 'site/privacy-policy'],
        ['pattern' => 'hashtag/<slug>', 'route' => 'hashtag/index'],
        ['pattern' => 'news', 'route' => 'news/index'],
        ['pattern' => 'news/<slug>', 'route' => 'news/view'],
        ['pattern' => 'events', 'route' => 'events/index'],
        ['pattern' => 'events/<slug>', 'route' => 'events/view'],
        ['pattern' => 'fund', 'route' => 'fund/index'],
        ['pattern' => 'problem', 'route' => 'problem/index'],
        ['pattern' => 'donate', 'route' => 'donate/index'],
        ['pattern' => 'shop', 'route' => 'shop/index'],
        ['pattern' => 'cart', 'route' => 'cart/index'],
        ['pattern' => 'specialists', 'route' => 'specialists/index'],
        ['pattern' => 'articles', 'route' => 'articles/index'],
        ['pattern' => 'articles/<slug>', 'route' => 'articles/view'],
    ]
];
