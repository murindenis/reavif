<?php

namespace frontend\controllers;

use frontend\models\search\MaterialSearch;
use common\models\Material;
use Yii;
use yii\web\Controller;

/**
 * Specialists controller
 */
class SpecialistsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $model = Material::find()
            ->where(['category_id' => Material::MATERIAL_NEWS, 'status' => Material::STATUS_ACTIVE])
            ->andWhere(['!=', 'user_id', 1])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        $news = [];
        foreach ($model as $key=>$item) {
            $news[date('d F',$item->created_at)][$key]['title'] = $item->getMaterialNameLang();
            $news[date('d F',$item->created_at)][$key]['slug'] = $item->slug;
            $news[date('d F',$item->created_at)][$key]['created_at'] = date('H:i',$item->created_at);
        }
        //d($news);die;
        return $this->render('index', [
            'news' => $news,
        ]);
    }

    public function actionView($slug)
    {
        $news = Material::find()->where(['slug' => $slug, 'status' => Material::STATUS_ACTIVE])->one();
        $lastNews = Material::find()->limit(3)->where(['category_id' => Material::MATERIAL_NEWS, 'status' => Material::STATUS_ACTIVE])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('view', [
            'news' => $news,
            'lastNews' => $lastNews,
        ]);
    }
}
