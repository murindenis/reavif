<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Material;

/**
 * Search controller
 */
class SearchController extends Controller
{
    public function actionIndex($search)
    {
        if (!empty($search)) {
            $materials = Material::find()
                ->orFilterWhere(['like', 'title_eng', $search])
                ->orFilterWhere(['like', 'title_esp', $search])
                ->andFilterWhere(['status' => Material::STATUS_ACTIVE])
                ->all();
            $materialsCount = Material::find()
                ->orFilterWhere(['like', 'title_eng', $search])
                ->orFilterWhere(['like', 'title_esp', $search])
                ->andFilterWhere(['status' => Material::STATUS_ACTIVE])
                ->count();
        }

        return $this->render('index', [
            'materials' => $materials ?? null,
            'materialsCount' => $materialsCount ?? 0,
        ]);
    }
}
