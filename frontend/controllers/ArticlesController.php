<?php

namespace frontend\controllers;

use frontend\models\search\MaterialSearch;
use common\models\Material;
use Yii;
use yii\web\Controller;

/**
 * Articles controller
 */
class ArticlesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new MaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['category_id' => Material::MATERIAL_ARTICLE, 'status' => Material::STATUS_ACTIVE])
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $dataProvider->pagination->pageSize = 10;
        $materials = $dataProvider->getModels();

        return $this->render('index', [
            'materials' => $materials,
            'pagination' => $dataProvider->pagination,
        ]);
    }

    public function actionView($slug)
    {
        $article = Material::find()->where(['slug' => $slug, 'status' => Material::STATUS_ACTIVE])->one();
        $lastArticles = Material::find()->limit(3)->where(['category_id' => Material::MATERIAL_ARTICLE, 'status' => Material::STATUS_ACTIVE])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('view', [
            'article' => $article,
            'lastArticles' => $lastArticles,
        ]);
    }
}
