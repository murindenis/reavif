<?php

namespace frontend\controllers;

use common\models\Cart;
use common\models\ShopOrders;
use common\models\ShopOrdersProduct;
use common\models\UserProfile;
use Yii;
use yii\web\Controller;
use common\models\ShopProduct;

/**
 * Shop controller
 */
class ShopController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $products = ShopProduct::find()->all();

        return $this->render('index', [
            'products' => $products,
        ]);
    }

    public function actionAddToCartAjax()
    {
        $userId = Yii::$app->request->post('user_id');
        $prodId = Yii::$app->request->post('product_id');

        if ($userId && $prodId) {
            $productNew = new Cart();
            $productNew->user_id = $userId;
            $productNew->shop_product_id = $prodId;
            $productNew->save();
        }
    }
    public function actionDropFromCartAjax()
    {
        $id = Yii::$app->request->post('id');

        if ($id) {
            (Cart::find()->where(['id' => $id])->one())->delete();
        }
    }
    public function actionOrderProductAjax()
    {
        $userId = Yii::$app->user->id;
        $cartIds  = Yii::$app->request->post('cart');

        $newOrder = new ShopOrders();
        $newOrder->user_id = $userId;

         if ($newOrder->save()) {
             foreach ($cartIds as $id) {
                 $newOrdersProduct = new ShopOrdersProduct();
                 $newOrdersProduct->shop_product_id = (int)$id;
                 $newOrdersProduct->shop_orders_id = (int)$newOrder->id;
                 $newOrdersProduct->save();
             }

             $cartProducts = Cart::find()->where(['user_id' => $userId])->all();
             foreach ($cartProducts as $item) {
                 $item->delete();
             }

         }
    }

    public function actionOrderContactAjax()
    {
        $userId = Yii::$app->user->id;
        $contacts = Yii::$app->request->post('UserProfile');
        /*var_dump('---');
        var_dump(Yii::$app->request->post('UserProfile')["firstname"]); die('123');*/

        $userProfile = UserProfile::find()->where(['user_id' => $userId])->one();
        $userProfile->updateAttributes([
             'firstname' => $contacts['firstname'],
             'lastname'  => $contacts['lastname'],
             'phone'     => $contacts['phone'],
             'country'   => $contacts['country'],
             'city'      => $contacts['city'],
             'street'    => $contacts['street'],
             'house'     => $contacts['house'],
             'apartment' => $contacts['apartment'],
        ]);
    }
}
