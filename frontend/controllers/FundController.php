<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Fund;
use common\models\Contacts;
use common\models\Partner;

/**
 * Fund controller
 */
class FundController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $fund = Fund::find()->where(['id' => 1])->one();
        $contacts = Contacts::find()->where(['id' => 1])->one();
        $partners = Partner::find()->all();

        return $this->render('index', [
            'fund'     => $fund,
            'contacts' => $contacts,
            'partners' => $partners,
        ]);
    }
}
