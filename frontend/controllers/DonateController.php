<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Donate;
use common\models\Condition;
use common\models\Rekvizit;

/**
 * Donate controller
 */
class DonateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $donate = Donate::find()->where(['id' => 1])->one();
        $requisites = Rekvizit::find()->where(['id' => 1])->one();

        return $this->render('index', [
            'donate' => $donate,
            'requisites' => $requisites,
        ]);
    }

    public function actionCondition()
    {
        $condition = Condition::find()->where(['id' => 1])->one();

        return $this->render('condition', [
            'condition'  => $condition,
        ]);
    }
}
