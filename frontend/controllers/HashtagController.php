<?php

namespace frontend\controllers;

use common\models\Hashtag;
use Yii;
use yii\web\Controller;
use common\models\Material;

/**
 * Hashtag controller
 */
class HashtagController extends Controller
{
    public function actionIndex($slug)
    {
        $hashtag = Hashtag::find()->where(['slug' => $slug])->one();
        return $this->render('index', [
            'materials' => $hashtag->materialHashtag,
            'hashtag' => $hashtag,
        ]);
    }
}
