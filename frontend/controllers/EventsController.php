<?php

namespace frontend\controllers;

use frontend\models\search\MaterialSearch;
use common\models\Material;
use Yii;
use yii\web\Controller;

/**
 * Events controller
 */
class EventsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new MaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['event_at' => SORT_DESC])->where(['category_id' => Material::MATERIAL_EVENTS, 'status' => Material::STATUS_ACTIVE])->all();
        $dataProvider->pagination->pageSize = 18;
        $events = $dataProvider->getModels();

        return $this->render('index', [
            'events' => $events,
            'pagination' => $dataProvider->pagination,
        ]);
    }

    public function actionView($slug)
    {
        $event = Material::find()->where(['slug' => $slug, 'status' => Material::STATUS_ACTIVE])->one();

        return $this->render('view', [
            'event' => $event,
        ]);
    }
}
