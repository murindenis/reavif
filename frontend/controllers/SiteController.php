<?php

namespace frontend\controllers;

use common\models\Hashtag;
use common\models\Material;
use common\models\Slider;
use common\models\Subscribe;
use frontend\models\ContactForm;
use PharIo\Manifest\Email;
use Psr\Log\NullLogger;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        /*$params = [
            'currency'    => 'EUR', // only support currency same PayPal
            'description' => 'Buy some item',
            'total_price' => 230,
            'email' => 'nguyentruongthanh.dn@gmail.com',
            'items' => [
                [
                    'name'     => 'Vinamilk',
                    'quantity' => 2,
                    'price'    => 100
                ],
                [
                    'name'     => 'Pepsi',
                    'quantity' => 3,
                    'price'    => 10
                ]
            ]
        ];
        $response = Yii::$app->payPalRest->getLinkCheckOut($params);*/
        //d($response);die;
        $slider = Slider::find()->all();
        $hashtags = Hashtag::find()->all() ?? null;

        $lastNews = Material::find()->where(['category_id' => Material::MATERIAL_NEWS, 'status' => Material::STATUS_ACTIVE])->limit(3)->orderBy(['id' => SORT_DESC])->all();
        $materialsBig = Material::find()->where(['status' => Material::STATUS_ACTIVE])->limit(2)->orderBy(['id' => SORT_DESC])->all();
        $materialsMin = Material::find()->where(['status' => Material::STATUS_ACTIVE])->limit(6)->orderBy(['id' => SORT_DESC])->all();

        $news = Material::find()->where(['category_id' => Material::MATERIAL_NEWS, 'status' => Material::STATUS_ACTIVE])->limit(3)->orderBy(['id' => SORT_DESC])->all();
        $events = Material::find()->where(['category_id' => Material::MATERIAL_EVENTS, 'status' => Material::STATUS_ACTIVE])->limit(3)->orderBy(['event_at' => SORT_DESC])->all();

        return $this->render('index', [
            'slider' => $slider,
            'hashtags'     => $hashtags,
            'lastNews'     => $lastNews,
            'materialsBig' => $materialsBig,
            'materialsMin' => $materialsMin,
            'news'         => $news,
            'events'       => $events,
        ]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options' => ['class' => 'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => \Yii::t('frontend', 'There was an error sending email.'),
                    'options' => ['class' => 'alert-danger']
                ]);
            }
        }

        return $this->render('contact', [
            'model' => $model
        ]);
    }

    public function actionPaypalSuccess() {
        d(1);
    }
    public function actionPaypalFiled() {
        d(0);
    }

    public function actionEmailAjax() {
        $email = Yii::$app->request->post('email');

        $model = Subscribe::find()->where(['email' => $email])->one();
        if ($model == Null) {
            $newEmail = new Subscribe();
            $newEmail->email = $email;
            $newEmail->save();
        }
    }

    public function actionPrivacyPolicy() {
        return $this->render('privacy-policy');
    }
}
