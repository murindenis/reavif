<?php

namespace frontend\controllers;

use common\models\Fund;
use common\models\Problem;
use Yii;
use yii\web\Controller;
use common\models\Material;
use yii\data\Pagination;

/**
 * Problem controller
 */
class ProblemController extends Controller
{
    public function actionIndex()
    {
        $problem = Problem::find()->where(['id' => 1])->one();
        $fund = Fund::find()->where(['id' => 1])->one();
        return $this->render('index', [
            'problem' => $problem,
            'fund'    => $fund,
        ]);
    }

}
