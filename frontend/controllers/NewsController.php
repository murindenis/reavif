<?php

namespace frontend\controllers;

use frontend\models\search\MaterialSearch;
use Yii;
use yii\web\Controller;
use common\models\Material;

/**
 * News controller
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new MaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['category_id' => Material::MATERIAL_NEWS, 'status' => Material::STATUS_ACTIVE])->orderBy(['created_at' => SORT_DESC])->all();
        $dataProvider->pagination->pageSize = 10;
        $model = $dataProvider->getModels();

        $news = [];
        foreach ($model as $key=>$item) {
            $news[date('d F',$item->created_at)][$key]['title'] = $item->getMaterialNameLang();
            $news[date('d F',$item->created_at)][$key]['slug'] = $item->slug;
            $news[date('d F',$item->created_at)][$key]['created_at'] = date('H:i',$item->created_at);
        }
        //d($news);die;
        return $this->render('index', [
            'news' => $news,
            'pagination' => $dataProvider->pagination,
        ]);
    }

    public function actionView($slug)
    {
        $news = Material::find()->where(['slug' => $slug, 'status' => Material::STATUS_ACTIVE])->one();
        $lastNews = Material::find()->limit(3)->where(['category_id' => Material::MATERIAL_NEWS])->orderBy(['id' => SORT_DESC])->all();

        return $this->render('view', [
            'news' => $news,
            'lastNews' => $lastNews,
        ]);
    }
}
