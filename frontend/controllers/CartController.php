<?php

namespace frontend\controllers;

use common\models\Cart;
use Yii;
use yii\web\Controller;
use common\models\ShopProduct;
use common\models\User;

/**
 * Cart controller
 */
class CartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $cart = Cart::find()->where(['user_id' => Yii::$app->user->id])->all();
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        return $this->render('index', [
            'cart' => $cart,
            'user' => $user,
        ]);
    }

}
