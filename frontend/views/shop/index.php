<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <section class="shop">
        <div class="container">
            <div class="list__product">
                <?php foreach ($products as $product) {?>
                <div class="product__item">
                    <div class="product__img">
                        <img src="<?= \common\behaviors\GlobalHelper::getImgPath($product)?>">
                    </div>
                    <div class="description__product">
                        <h3 class="title__product"><?= $product->getProductNameLang()?></h3>
                        <h3 class="title__product"><?= $product->price?></h3>
                            <?php if (!Yii::$app->user->isGuest) {?>
                            <button data-product_id="<?= $product->id?>"
                                    data-user_id="<?= Yii::$app->user->id?>"
                                    data-text="<?= Yii::t('frontend', 'Success add to cart')?>"
                                    class="add-to-cart">
                                <?= Yii::t('frontend', 'Add to Cart')?>
                            </button>
                            <? } ?>
                    </div>
                </div>
                <? } ?>
            </div>
        </div>
    </section>