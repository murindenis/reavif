<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<div id="pages-body">
    <section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <div class="container">
        <h1 class="fund_title">#<?= $hashtag->getHashtagNameLang()?></h1>
    </div>
</section>
    <section class="fund-section">
    <div class="container">
        <div class="fund-wrapper">
            <div class="fund-content">
                <div class="result__list">
                    <?php if (!empty($materials)) {?>
                        <?php foreach ($materials as $material) { ?>
                            <?php if($material->material->status == \common\models\Material::STATUS_ACTIVE) {?>
                            <div class="result__item" href="#">
                                <a href="<?= $material->material->getMaterialUrl($material->material->category_id,$material->material->slug)?>" class="result__img">
                                    <img src="<?= \common\behaviors\GlobalHelper::getImgPath($material->material)?>">
                                </a>
                                <div href="#" class="result__content">
                                    <a href="<?= $material->material->getMaterialUrl($material->material->category_id,$material->material->slug)?>" class="result__content__title"><p class="news-title"><?= $material->material->getMaterialNameLang()?></p></a>
                                    <ul class="news-hashtag">
                                        <?php foreach ($material->material->materialHashtag as $hashtag) { ?>
                                            <li><a href="<?= \yii\helpers\Url::toRoute(['/hashtag/index', 'slug' => $hashtag->hashtag->slug])?>"><?= $hashtag->hashtag->getHashtagNameLang()?></a></li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </div>
                            <? } ?>
                        <? } ?>
                    <? } ?>
                    <!--<button type="button" class="btn btn-more">Show more</button>-->
                </div>
            </div>
            <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>
        </div>
    </div>
</section>
</div>