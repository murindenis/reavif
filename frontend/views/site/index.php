<?php $this->title = 'REAVIF – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<div id="base-body">
    <section class="main-section">
        <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
        <div class="slider-section">
            <div class="slider-block">
                <?php foreach ($slider as $slide) { //d($item->material->materialHashtag);die;?>
                <div class="slider-item" style="background: url(<?= \common\behaviors\GlobalHelper::getImgPath($slide)?>)no-repeat;background-size: cover;background-position: center center;">
                    <div class="slider-text">
    <!--                    <p class="slider-opinion">-->
    <!--                        <span>-->
    <!--                            REAVIF --->
    <!--                        </span>--><?//= $slide->title?><!--</p>-->
                        <p class="slider-opinion">
                            <?= $slide->title?>
                        </p>
                    </div>
                    <div id="mask"></div>
                </div>
                <? } ?>
            </div>
            <div class="arrows-slider">
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="22" viewBox="0 0 25 22" class="prev_arrows"><path fill-rule="evenodd" fill="#fff" d="M14 21.992l-2.845-2.846 6.131-6.135H-.003V8.985h17.289l-6.131-6.134L14 .004l10.985 10.994L14 21.992z"></path></svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="22" viewBox="0 0 25 22" class="next_arrows"><path fill-rule="evenodd" fill="#fff" d="M14 21.992l-2.845-2.846 6.131-6.135H-.003V8.985h17.289l-6.131-6.134L14 .004l10.985 10.994L14 21.992z"></path></svg>
            </div>
        </div>
    </section>
    <section class="news">
        <div class="container">
            <?= \yii\helpers\Html::a(Yii::t('frontend', 'News'), ['/news/index'], ['class' => 'title-news'])?>
            <div class="news-list">
                <?php foreach ($lastNews as $item) {?>
                    <div class="news-item">
                        <?= \yii\helpers\Html::a($item->getMaterialNameLang(), ['/news/view', 'slug' => $item->slug])?>
                        <p class="news_date"><?= date('d M Y',$item->created_at)?></p>
                    </div>
                <? } ?>
            </div>
        </div>
    </section>
    <section class="dispatch">
        <div class="container">
            <div class="dispatch-content">
                <span><?= Yii::t('frontend', 'Subscribe to be informed')?></span>
                <form id="subscribeForm">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
                    <input type="text" placeholder="<?= Yii::t('frontend', 'Your e-mail')?>" class="email-input" name="email">
                    <button type="submit" class="btn-next"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="17" viewBox="0 0 25 22"><path fill-rule="evenodd" fill="#fff" d="M14 21.992l-2.845-2.846 6.131-6.135H-.003V8.985h17.289l-6.131-6.134L14 .004l10.985 10.994L14 21.992z" class="asd"></path></svg></button>
                </form>
                <span>
                    <i class="fa fa-heart fa-heart-animate" aria-hidden="true"></i>
                    <?= Yii::t('frontend', 'Help fund')?>
                </span>
            </div>
        </div>
    </section>
    <section class="slider-words-section">
        <div class="slider-words-block">
            <?php foreach ($hashtags as $hashtag) {?>
            <div class="words-item"><p><?= \yii\helpers\Html::a($hashtag->getHashtagNameLang(), ['/hashtag/index', 'slug' => $hashtag->slug])?></p></div>
            <? } ?>
        </div>
    </section>
        <section class="news-section">
            <div class="container">
                <div class="news-list">
                    <?php foreach ($materialsBig as $key=>$material) {?>
                        <div class="news-item-l <?= !$key ? 'item-l--red' : ''?>">
                            <a href="<?= \yii\helpers\Url::toRoute(['/news/view', 'slug' => $material->slug])?>">
                                <div class="news-img">
                                    <img src="<?= \common\behaviors\GlobalHelper::getImgPath($material)?>">
                                </div>
                                <p class="news-title">
                                    <span><?= $material->getMaterialCategoryNameLang()?> - </span>
                                    <?= $material->getMaterialNameLang()?>
                                </p>
                                <p class="news-desc">
                                    <?= $material->getMaterialIntrotextLang()?>
                                </p>
                            </a>
                            <ul class="news-hashtag">
                                <?php foreach ($material->materialHashtag as $materialHashtag){ ?>
                                    <li><?= \yii\helpers\Html::a($materialHashtag->hashtag->getHashtagNameLang(),
                                            ['/hashtag/index', 'slug' => $materialHashtag->hashtag->slug])?>
                                    </li>
                                <? } ?>
                            </ul>
                        </div>
                    <? } ?>
                </div>
            </div>
        </section>

            <section class="news-section">
                <div class="container">
                    <div class="news-list">
                        <div class="news-col">
                            <?php foreach ($materialsMin as $key=>$item) {?>
                                <?php if ($key == 2 || $key == 3) {?>
                                <div class="news-col-item">
                                    <a href="<?= $item->getMaterialUrl($item->category_id, $item->slug) ?>">
                                        <div class="news-img news-img-sm">
                                            <img src="<?= \common\behaviors\GlobalHelper::getImgPath($item)?>">
                                        </div>
                                        <p class="news-title"><span><?= $item->getMaterialCategoryNameLang()?> - </span><?= $item->getMaterialNameLang()?></p>
                                        <p class="news-desc"><?= $item->getMaterialIntrotextLang()?></p>
                                    </a>
                                    <ul class="news-hashtag">
                                        <?php foreach ($material->materialHashtag as $materialHashtag){ ?>
                                            <li><?= \yii\helpers\Html::a($materialHashtag->hashtag->getHashtagNameLang(),
                                                    ['/hashtag/index', 'slug' => $materialHashtag->hashtag->slug])?></li>
                                        <? } ?>
                                    </ul>
                                </div>
                                <? }?>
                            <? } ?>
                        </div>

                        <div class="news-col">
                            <?php foreach ($materialsMin as $key=>$item) {?>
                                <?php if ($key == 4 || $key == 5) {?>
                                    <div class="news-col-item">
                                        <a href="<?= $item->getMaterialUrl($item->category_id, $item->slug) ?>">
                                            <div class="news-img news-img-sm">
                                                <img src="<?= \common\behaviors\GlobalHelper::getImgPath($item)?>">
                                            </div>
                                            <p class="news-title"><span><?= $item->getMaterialCategoryNameLang()?> - </span><?= $item->getMaterialNameLang()?></p>
                                            <p class="news-desc"><?= $item->getMaterialIntrotextLang()?></p>
                                        </a>
                                        <ul class="news-hashtag">
                                            <?php foreach ($material->materialHashtag as $materialHashtag){ ?>
                                                <li><?= \yii\helpers\Html::a($materialHashtag->hashtag->getHashtagNameLang(),
                                                        ['/hashtag/index', 'slug' => $materialHashtag->hashtag->slug])?></li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                <? }?>
                            <? } ?>
                        </div>

                        <div class="news-col">
                            <div id="fb-root"></div>
                            <div class="fb-page" data-href="https://www.facebook.com/reavif/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>

                            <a href="https://twitter.com/reavif?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @reavif</a>
                        </div>
                    </div>
                </div>
            </section>

    <!--<section class="news-slider">
        <div class="container">
            <div class="news-slider__content">
                <a href="#" class="hashtag-link">Actuality</a>
                <ul class="news-slider__list">
                    <li><a href="#">Manchek: Opening gallery</a></li>
                    <li><a href="#">Final football cup</a></li>
                    <li><a href="#">Open festival in Madrid</a></li>
                </ul>
            </div>
        </div>
        <div class="news-slider__img">
            <img src="img/news_slider_img.jpg">
        </div>
    </section>-->
    <section class="calendar-section">
        <div class="container">
            <a href="<?= \yii\helpers\Url::toRoute(['/events/index'])?>" class="calendar-content"><?= Yii::t('frontend', 'Calendar')?></a>
            <div class="calendar-list">
                <?php foreach ($events as $item) {?>
                <div class="calendar-item">
                    <div class="calendar-item__img">
                        <a href="<?= \yii\helpers\Url::toRoute(['/events/view', 'slug' => $item->slug])?>">
                            <img src="<?= \common\behaviors\GlobalHelper::getImgPath($item)?>">
                        </a>
                        <div class="data">
                            <p class="calendar-number"><?= date('d', $item->event_at)?></p>
                            <p class="calendar-month"><?= date('M', $item->event_at)?></p>
                        </div>
                    </div>
                    <a href="<?= \yii\helpers\Url::toRoute(['/events/view', 'slug' => $item->slug])?>">
                        <p class="calendar-title"><?= $item->getMaterialNameLang()?></p>
                        <p class="calendar-desc"><?= $item->getMaterialIntrotextLang()?></p>
                    </a>
                </div>
                <? } ?>
            </div>
        </div>
    </section>
    <section class="opinion-section">
        <div class="container">
            <div class="opinion-content">
                <div class="opinion-list">
                    <?php foreach ($news as $item) {?>
                    <div class="opinion-col-item">
                        <a href="<?= \yii\helpers\Url::toRoute(['/news/view', 'slug' => $item->slug])?>" class="opinion-news">
                            <div class="news-img">
                                <img src="<?= \common\behaviors\GlobalHelper::getImgPath($item)?>">
                            </div>
                        </a>
                        <div class="opinion-news">
                            <a href="<?= \yii\helpers\Url::toRoute(['/news/view', 'slug' => $item->slug])?>">
                                <p class="opinion-title"><span><?= $item->hashtag->getHashtagNameLang()?> - </span><?= $item->getMaterialNameLang()?></p>
                                <p class="opinion-desc"><?= $item->getMaterialIntrotextLang()?></p>
                            </a>
                            <ul class="news-hashtag">
                                <?php foreach ($item->materialHashtag as $materialHashtag){ ?>
                                    <li><?= \yii\helpers\Html::a($materialHashtag->hashtag->getHashtagNameLang(),
                                            ['/hashtag/index', 'slug' =>$materialHashtag->hashtag->slug])?>
                                    </li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                    <? } ?>
                    <?= \yii\helpers\Html::a(Yii::t('frontend', 'Show more'), ['/news/index'], ['class' => 'btn btn-more', 'style' => 'padding:18px 0'])?>
                    <!--<button type="button" class="btn btn-more">Show more</button>-->
                </div>
                <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>
            </div>
        </div>
    </section>
</div>