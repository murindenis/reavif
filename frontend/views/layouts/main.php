<?php
use yii\helpers\Url;
/* @var $this \yii\web\View */

/* @var $content string */

$this->beginContent('@frontend/views/layouts/reavif.php'); ?>

<? echo $content; ?>

    <footer>
        <div class="container">
            <div class="footer-content">
                <nav class="footer-nav">
                    <div class="footer-nav__top">
                        <ul>
                            <li><a href="<?= Url::toRoute(['/problem/index']) ?>"><?= Yii::t('frontend', 'Problem')?></a></li>
                            <li><a href="<?= Url::toRoute(['/fund/index']) ?>"><?= Yii::t('frontend', 'Fund')?></a></li>
                            <li><a href="<?= Url::toRoute(['/donate/index']) ?>" class="btn btn-donate"><?= Yii::t('frontend', 'Donate')?></a></li>
                        </ul>
                    </div>
                </nav>
                <nav class="footer-nav footer-nav__bottom">
                    <div class="footer-nav__top">
                        <ul>
                            <li><a href="<?= Url::toRoute(['/news/index']) ?>"><?= Yii::t('frontend', 'News')?></a></li>
                            <li><a href="<?= Url::toRoute(['/events/index']) ?>"><?= Yii::t('frontend', 'Events')?></a></li>
                            <li><a href="<?= Url::toRoute(['/articles/index']) ?>"><?= Yii::t('frontend', 'Articles')?></a></li>
<!--                            <li><a href="--><?//= Url::toRoute(['/specialists/index']) ?><!--">--><?//= Yii::t('frontend', 'Specialists')?><!--</a></li>-->
                            <li><a href="<?= Url::toRoute(['/donate/condition']) ?>"><?= Yii::t('frontend', 'Terms of use')?> & <?= Yii::t('frontend', 'Privacy Policy')?></a></li>
                            <!--<li><a href="<?/*= Url::toRoute(['/site/privacy-policy']) */?>"><?/*= Yii::t('frontend', 'Privacy Policy')*/?></a></li>-->
                            <!--<li><a href="#"><?/*= Yii::t('frontend', 'Information')*/?></a></li>
                            <li><a href="#"><?/*= Yii::t('frontend', 'Specialists')*/?></a></li>-->
                            <!--<li><a href="<?/*= Url::toRoute(['/shop/index']) */?>"><?/*= Yii::t('frontend', 'Shop')*/?></a></li>-->
                            <!--<li><a href="text__page.html"><?/*= Yii::t('frontend', 'Terms of Use')*/?></a></li>-->
                        </ul>
                    </div>
                </nav>
                <?php $socials = \common\models\Social::find()->all()?>
                <ul class="social">
                    <?php foreach ($socials as $social) { ?>
                        <?php if (!empty($social->link) && $social->name == 'facebook') {?>
                            <li><a <!--href="--><?/*= $social->link */?>"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                        <? } ?>
                    <? } ?>
                    <?php foreach ($socials as $social) { ?>
                        <?php if (!empty($social->link) && $social->name == 'twitter') {?>
                            <li><a <!--href="--><?/*= $social->link */?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <? } ?>
                    <? } ?>
                    <?php foreach ($socials as $social) { ?>
                        <?php if (!empty($social->link) && $social->name == 'vk') {?>
                            <li><a <!--href="--><?/*= $social->link */?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                        <? } ?>
                    <? } ?>
                    <?php foreach ($socials as $social) { ?>
                        <?php if (!empty($social->link) && $social->name == 'instagram') {?>
                            <li><a <!--href="--><?/*= $social->link */?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <? } ?>
                    <? } ?>
                    <?php foreach ($socials as $social) { ?>
                        <?php if (!empty($social->link) && $social->name == 'youtube') {?>
                            <li><a <!--href="--><?/*= $social->link */?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        <? } ?>
                    <? } ?>
                </ul>
                <div class="footer-content__bottom">
                    <p><?= Yii::t('frontend', 'All rights reserved')?></p>
                    <p>REAVIF – Fundacion - Readaptacion para la Vida&nbsp;&copy;&nbsp;2018</p>
                </div>
            </div>
        </div>
    </footer>
    <?php //if (\Yii::$app->controller->action->id == 'index') { ?>
    <div class="search__pop-up">
        <i class="fa fa-times close__pop-up"></i>
        <div class="search__wrapper">
            <input type="text" placeholder="Поиск" value="">
            <button>
                <i class="fa fa-search" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <? //} ?>
    <div class="notification"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=1897604370554907&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script>

        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://reavif-org.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>



<? $this->endContent(); ?>