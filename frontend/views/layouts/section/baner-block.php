<div class="banner-block">
    <div class="banner-news">
        <img src="/img/banner-02.gif">
        <div class="banner-content">
            <p class="banner-title"><?= Yii::t('frontend', 'Do you need help?')?></p>
            <div class="banner-message">
                <a href="mailto:info@REAVIF.org" class="banner-info">info@REAVIF.org</a><p></p>
            </div>
        </div>
    </div>
</div>