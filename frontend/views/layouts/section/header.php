<header>
    <div class="logo">
        <!-- <svg class="icon-logo-c icon-white"><use xlink:href="./img/sprite.svg#icon-logo"></use></svg>				 -->
        <!-- <svg class="icon-logo-c icon-logo-white"><use xlink:href="img/sprite.svg#icon-icon-reavif-new"></use></svg>				 -->
        <a class="logo-l" href="<?= \yii\helpers\Url::toRoute(['/site/index'])?>"><img src="/img/icon-reavif-new.svg"></a>
        <a class="logo-sm" href="<?= \yii\helpers\Url::toRoute(['/site/index'])?>"><img src="/img/icon-reavif-sm.svg"></a>
    </div>
    <div class="navigation">
        <div class="navigation-top">
            <ul class="nav-top">
                <li><a href="<?= \yii\helpers\Url::toRoute(['/problem/index'])?>"><?= Yii::t('frontend', 'Relevancia')?></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/fund/index'])?>"><?= Yii::t('frontend', 'About fund')?></a></li>
            </ul>
            <ul class="nav-bottom">
                <li class="logo logo-mobile">
                    <svg class="icon-logo-c icon-white"><use xlink:href="img/sprite.svg#icon-logo"></use></svg>
                </li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/search/index', 'search' => ''])?>"><i class="fa fa-search open__pop-up" aria-hidden="true"></i></a></li>
                <!--<li><a href="--><?//= \yii\helpers\Url::toRoute(['/events/index'])?><!--"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></a></li>-->
                <!--<li><a href="<?/*= \yii\helpers\Url::toRoute(['/cart/index'])*/?>"><i class="fa fa-shopping-basket"></i></a></li>-->
                <?php if (Yii::$app->user->isGuest) {?>
                    <li><?= \yii\helpers\Html::a('<i class="fa fa-sign-in"></i>', '/user/sign-in/login')?></li>
                <? } else {?>
                    <li><?= \yii\helpers\Html::a('<i class="fa fa-user"></i>', '/user/default/index')?></li>
                    <li><?= \yii\helpers\Html::a('<i class="fa fa-sign-out"></i>', '/user/sign-in/logout', ['data-method' => 'post'])?></li>

                <? }?>
                <? $code = Yii::$app->language?>
                <li class="lang">
                    <a href="<?= \yii\helpers\Url::toRoute(['/site/set-locale', 'locale' => $code == \common\behaviors\LanguageHelper::LANG_ESP ? 'es' : 'en-US'])?>" class="lang-link--active">
                        <?= $code == \common\behaviors\LanguageHelper::LANG_ESP ? 'ES' : 'EN'?>
                    </a>
                    <div class="lang-dropdown">
                        <a href="<?= \yii\helpers\Url::toRoute(['/site/set-locale', 'locale' => $code == \common\behaviors\LanguageHelper::LANG_ESP ? 'en-US' : 'es'])?>">
                            <?= $code == \common\behaviors\LanguageHelper::LANG_ESP ? 'EN' : 'ES'?>
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-donate btn-donate--header" href="<?= \yii\helpers\Url::toRoute(['/donate/index'])?>">
                        <?= Yii::t('frontend', 'Donate')?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navigation-bottom">
            <ul class="nav-top">
                <li><a href="<?= \yii\helpers\Url::toRoute(['/news/index'])?>"><?= Yii::t('frontend', 'News')?></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/events/index'])?>"><?= Yii::t('frontend', 'Events')?></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/articles/index'])?>"><?= Yii::t('frontend', 'Articles')?></a></li>
                <!--<li><a href="#"><?/*= Yii::t('frontend', 'Information')*/?></a></li>-->
                <!--<li><a href="<?= \yii\helpers\Url::toRoute(['/specialists/index'])?>"><?= Yii::t('frontend', 'Specialists')?></a></li>-->
                <!--<li><a href="<?/*= \yii\helpers\Url::toRoute(['/shop/index'])*/?>"><?/*= Yii::t('frontend', 'Shop')*/?></a></li>-->
            </ul>
        </div>
        <div class="btn-close">
            <div class="fa fa-close"></div>
        </div>
        <div class="navigation-mobile">
            <ul class="nav-mobile">
                <li>
                    <ul class="list__icons">
                        <li><a href="#"><i class="fa fa-search open__pop-up" aria-hidden="true"></i> <?= Yii::t('frontend', 'Search')?></a></li>
<!--                        <li><a href="--><?//= \yii\helpers\Url::toRoute(['/events/index'])?><!--"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></a></li>-->
<!--                        <li><a href="--><?//= \yii\helpers\Url::toRoute(['/cart/index'])?><!--"><i class="fa fa-shopping-basket"></i></a></li>-->
                    </ul>
                </li>
                <li><a href="#"></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/problem/index'])?>"><?= Yii::t('frontend', 'Relevancia')?></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/fund/index'])?>"><?= Yii::t('frontend', 'About fund')?></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/news/index'])?>"><?= Yii::t('frontend', 'News')?></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/events/index'])?>"><?= Yii::t('frontend', 'Events')?></a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/articles/index'])?>"><?= Yii::t('frontend', 'Articles')?></a></li>
                <!--<li><a href="#"><?/*= Yii::t('frontend', 'Information')*/?></a></li>-->
<!--                <li><a href="--><?//= \yii\helpers\Url::toRoute(['/specialists/index'])?><!--">--><?//= Yii::t('frontend', 'Specialists')?><!--</a></li>-->
                <!--<li><a href="#"><?/*= Yii::t('frontend', 'Shop')*/?></a></li>-->
                <li><a class="btn btn-donate btn-donate--header" href="<?= \yii\helpers\Url::toRoute(['/donate/index'])?>"><?= Yii::t('frontend', 'Donate')?></a></li>
            </ul>
            <ul class="nav-mobile-lang">
                <li><a href="<?= \yii\helpers\Url::toRoute(['/site/set-locale', 'locale' => \common\behaviors\LanguageHelper::LANG_ESP])?>">ES</a></li>
                <li><a href="<?= \yii\helpers\Url::toRoute(['/site/set-locale', 'locale' => \common\behaviors\LanguageHelper::LANG_ENG])?>">EN</a></li>
            </ul>
        </div>
    </div>
    <div class="btn-hamburger">
        <div class="fa fa-bars"></div>
    </div>
</header>