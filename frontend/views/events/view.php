<?php $this->title = 'Reavif – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<section class="help help-detail">
    <div class="help-detail__img" style="background: url(<?= \common\behaviors\GlobalHelper::getImgPath($event)?>) no-repeat center center; background-size: 100%">
        <div class="help-detail__overlay">
            <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
            <div class="container">
                <div class="main__title__news">
                    <h1><?= $event->getMaterialNameLang()?></h1>
                    <div class="data_news">
                        <a href="<?= \yii\helpers\Url::toRoute(['/events/index'])?>">Calendar</a>
                        <p><?= date('d M Y', $event->created_at)?></p>
                    </div>
                </div>
                <div class="bottom_info_title_news phone-hidden">
                    <?php foreach ($event->materialHashtag as $hashtag) {?>
                        <a href="<?= \yii\helpers\Url::toRoute(['/hashtag/view', 'slug' => $hashtag->hashtag->slug])?>" class="hachtage_title"><?= $hashtag->hashtag->getHashtagNameLang()?></a>
                    <? }?>
                </div>

            </div>
        </div>
    </div>
    <div class="date__block">
        <span class="date__block__number"><?= date('d', $event->event_at)?></span>
        <span class="date__block__month"><?= date('F Y', $event->event_at)?></span>
        <span class="date__block__day"><?= date('l', $event->event_at)?></span>
    </div>
</section>
<section class="news__item__wrapper">
    <div class="container">
        <div class="news_content">
            <h4><?= $event->getMaterialIntrotextLang()?></h4>
            <p><?= $event->getMaterialDescriptionLang()?></p>
            <div class="place">
                <p class="title__place"><?= Yii::t('frontend', 'Place')?></p>
                <p class="name__place"><?= $event->getMaterialPlaceLang()?></p>
            </div>
            <div id="disqus_thread"></div>
            <!-- <span>Subscribe to our page <a href="#">REAVIF</a> in Facebook</span> -->

            <!--<ul class="social__list">
                <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a></li>
            </ul>-->
        </div>
    </div>
</section>
