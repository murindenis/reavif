<?php $this->title = 'REAVIF – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<div id="pages-body">
    <section class="help">
        <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
        <div class="container">
            <h1 class="fund_title"><?= Yii::t('frontend', 'Calendar')?></h1>
        </div>
    </section>
    <section class="calendar_page">
        <!--<div class="container">
            <div class="calendar_date_list">
                <div class="calendar_date_item">
                    <select>
                        <option data-display="Month">Month</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>
                <div class="calendar_date_item">
                    <select>
                        <option data-display="Year">Year</option>
                        <option value="1">2018</option>
                        <option value="2">2019</option>
                        <option value="3">2020</option>
                        <option value="4">2021</option>
                    </select>
                </div>
                <div class="calendar_date_item">
                    <select>
                        <option data-display="Event">Event</option>
                        <option value="1">Culture</option>
                        <option value="2">Sport</option>
                        <option value="3">Oragnization</option>
                    </select>
                </div>
                <div class="calendar_date_item_button">
                    <button class="btn btn-donate">SHOW</button>
                </div>
            </div>
        </div>
        <hr class="hr-div">-->
        <div class="wrapper">
            <div class="container">
                <div class="calendar-list">
                    <?php if (!empty($events)) {?>
                        <?php foreach ($events as $item) {?>
                        <div class="calendar-item">
                            <div class="calendar-item__img">
                                <a href="<?= \yii\helpers\Url::toRoute(['/events/view', 'slug' => $item->slug])?>">
                                    <img src="<?= \common\behaviors\GlobalHelper::getImgPath($item)?>"></a>
                                <div class="data">
                                    <p class="calendar-number"><?= date('d', $item->event_at)?></p>
                                    <p class="calendar-month"><?= date('M', $item->event_at)?></p>
                                </div>
                            </div>
                            <a href="<?= \yii\helpers\Url::toRoute(['/events/view', 'slug' => $item->slug])?>">
                                <p class="calendar-title"><?= $item->getMaterialNameLang()?></p>
                                <p class="calendar-desc"><?= $item->getMaterialIntrotextLang()?></p>
                            </a>
                        </div>
                        <? } ?>
                    <? } ?>
                </div>
            </div>
            <!-- Pagination -->
            <div>
                <?= \yii\widgets\LinkPager::widget(['pagination' => $pagination]) ?>
            </div>
        </div>
    </section>
</div>