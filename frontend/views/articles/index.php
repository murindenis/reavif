<?php $this->title = 'REAVIF – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<div id="pages-body">
    <section class="help">
        <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
        <div class="container">
            <h1 class="fund_title"><?= Yii::t('frontend', 'Articles')?></h1>
        </div>
    </section>
    <section class="fund-section">
        <div class="container">
            <div class="fund-wrapper">
                <div class="fund-content">
                    <div class="result__list">
                        <?php if (!empty($materials)) {?>
                            <?php foreach ($materials as $material) {?>
                                <div class="result__item" href="#">
                                    <a href="<?= $material->getMaterialUrl($material->category_id,$material->slug)?>" class="result__img">
                                        <img src="<?= \common\behaviors\GlobalHelper::getImgPath($material)?>">
                                    </a>
                                    <div href="#" class="result__content">
                                        <a href="<?= $material->getMaterialUrl($material->category_id,$material->slug)?>" class="result__content__title"><p class="news-title"><?= $material->getMaterialNameLang()?></p></a>
                                        <ul class="news-hashtag">
                                            <?php foreach ($material->materialHashtag as $hashtag) { ?>
                                                <li><a href="<?= \yii\helpers\Url::toRoute(['/hashtag/index', 'slug' => $hashtag->hashtag->slug])?>"><?= $hashtag->hashtag->getHashtagNameLang()?></a></li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            <? } ?>
                            <div>
                                <?= \yii\widgets\LinkPager::widget(['pagination' => $pagination]) ?>
                            </div>
                        <? } ?>
                        <!--<button type="button" class="btn btn-more">Show more</button>-->
                    </div>
                </div>
                <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>
            </div>
        </div>
    </section>
</div>