<?php $this->title = 'Reavif – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<section class="help help-detail">
    <div class="help-detail__img" style="background: url(<?= \common\behaviors\GlobalHelper::getImgPath($article)?>) no-repeat center center; background-size: 100%">
        <div class="help-detail__overlay">
            <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
            <div class="container">
                <div class="main__title__news">
                    <h1><?= $article->getMaterialNameLang()?></h1>
                    <div class="data_news">
                        <a><?= $article->getMaterialCategoryNameLang()?></a>
                        <p><?= date('d M Y', $article->created_at)?></p>
                    </div>
                </div>
                <div class="bottom_info_title_news phone-hidden">
                    <?php foreach ($article->materialHashtag as $hashtag) {?>
                        <a href="<?= \yii\helpers\Url::toRoute(['/hashtag/index', 'slug' => $hashtag->hashtag->slug])?>" class="hachtage_title"><?= $hashtag->hashtag->getHashtagNameLang()?></a>
                    <? }?>
                    <!--<a href="index.html" class="index-link">
                        REAVIF
                    </a>-->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="news__item__wrapper">
    <div class="container">
        <div class="news_content">
            <h4><?= $article->getMaterialIntrotextLang()?></h4>
            <p><?= $article->getMaterialDescriptionLang()?></p>
            <div id="disqus_thread"></div>
            <!-- <span>Subscribe to our page <a href="#">REAVIF</a> in Facebook</span> -->

            <!--<ul class="social__list">
                <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a></li>
            </ul>-->
        </div>
    </div>
    <!-- <div class="social__news__page">
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" class="_306Ss"><path fill-rule="evenodd" d="M28 32h-7.975V19.987h4.388l.657-3.979h-5.045V12.16c0-1.267.339-2.168 2.156-2.168l2.835.001V6.201c-.401-.053-1.243-.226-2.844-.226-3.342 0-6.177 2.224-6.177 5.97v4.063h-3.997v3.979h3.996V32H4c-2.209 0-4-1.791-4-4V4c0-2.209 1.791-4 4-4h24c2.209 0 4 1.791 4 4v24c0 2.209-1.791 4-4 4zm-7.975 0h-4.031 4.031z"></path></svg>
        </a>
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="41" height="24" viewBox="0 0 41 24" class="_2U8E1" data-reactid="109"><path fill-rule="evenodd" d="M19.647 23.646h2.398s.725-.08 1.095-.479c.341-.367.33-1.055.33-1.055s-.047-3.224 1.447-3.699c1.473-.468 3.364 3.116 5.369 4.494 1.515 1.043 2.667.814 2.667.814l5.36-.075s2.804-.173 1.475-2.381c-.109-.18-.775-1.633-3.986-4.618-3.361-3.124-2.91-2.619 1.138-8.023 2.466-3.291 3.451-5.3 3.143-6.161-.293-.819-2.107-.603-2.107-.603l-6.035.038s-.448-.062-.779.137c-.325.194-.533.649-.533.649s-.955 2.547-2.229 4.713c-2.687 4.569-3.762 4.811-4.201 4.527-1.022-.662-.766-2.657-.766-4.074 0-4.43.67-6.276-1.307-6.754-.656-.159-1.139-.264-2.817-.281-2.153-.022-3.975.007-5.007.513-.687.337-1.217 1.087-.894 1.13.399.054 1.302.245 1.781.897.619.843.597 2.736.597 2.736s.355 5.213-.83 5.86c-.814.445-1.929-.462-4.325-4.609-1.227-2.123-2.154-4.471-2.154-4.471s-.179-.439-.498-.674c-.387-.284-.927-.375-.927-.375l-5.735.038s-.861.024-1.177.399c-.281.334-.023 1.024-.023 1.024s4.49 10.52 9.574 15.821c4.662 4.862 9.956 4.542 9.956 4.542z" data-reactid="110"></path></svg>
        </a>
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="42" height="32" viewBox="0 0 42 32" class="_5Qqiw" data-reactid="113"><path fill-rule="evenodd" d="M41.024 3.78c-1.474.643-3.057 1.078-4.719 1.273 1.696-1 2.999-2.584 3.612-4.472-1.587.926-3.346 1.599-5.217 1.961C33.201.971 31.066-.01 28.703-.01c-5.304 0-9.202 4.869-8.004 9.926C13.871 9.579 7.817 6.36 3.765 1.469 1.612 5.102 2.648 9.856 6.307 12.262c-1.347-.042-2.614-.406-3.721-1.011-.09 3.745 2.638 7.247 6.59 8.028-1.157.31-2.423.381-3.711.138 1.046 3.212 4.08 5.549 7.676 5.614-3.455 2.663-7.805 3.853-12.165 3.348 3.636 2.293 7.955 3.631 12.595 3.631 15.254 0 23.872-12.676 23.353-24.046 1.605-1.139 2.998-2.563 4.1-4.184z" data-reactid="114"></path></svg>
        </a>
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="19" viewBox="0 0 12 19" class="_3BGlo" data-reactid="117"><path fill-rule="evenodd" d="M10.836 12.293c-.886.542-1.849.926-2.847 1.147l2.741 2.668c.562.546.562 1.432 0 1.977-.562.546-1.47.546-2.031 0l-2.695-2.621-2.692 2.621c-.282.273-.649.409-1.018.409-.367 0-.734-.136-1.015-.409-.56-.545-.56-1.431 0-1.977L4.02 13.44c-.998-.221-1.961-.606-2.847-1.147-.672-.412-.873-1.274-.45-1.929.422-.654 1.308-.85 1.98-.439 2.008 1.229 4.593 1.229 6.602 0 .673-.411 1.56-.215 1.981.439.423.654.221 1.517-.45 1.929zM6.004 9.547c-2.703 0-4.903-2.139-4.903-4.77S3.301.004 6.004.004c2.705 0 4.904 2.142 4.904 4.773s-2.199 4.77-4.904 4.77zm0-6.746c-1.118 0-2.031.886-2.031 1.976 0 1.089.913 1.975 2.031 1.975 1.12 0 2.03-.886 2.03-1.975 0-1.09-.91-1.976-2.03-1.976z" data-reactid="118"></path></svg>
        </a>
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="30" viewBox="0 0 36 30" class="_1Ssce" data-reactid="121"><path fill-rule="evenodd" d="M33.696.105L.769 12.808c-1.067.411-1.002 1.926.096 2.247l8.367 2.446 3.123 9.81c.325 1.024 1.637 1.335 2.396.568l4.326-4.373 8.488 6.168c1.039.755 2.518.194 2.782-1.055L35.969 2.01c.275-1.303-1.02-2.389-2.273-1.905zm-4.129 5.904L14.273 19.4c-.152.133-.249.318-.272.518l-.589 5.182c-.019.169-.258.192-.309.029L10.68 17.4c-.11-.353.034-.737.352-.931L29.093 5.378c.415-.255.841.311.474.631z" data-reactid="122"></path></svg>
        </a>
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19" class="_2r5yb" data-reactid="125"><path fill-rule="evenodd" d="M9.539 18.838h-.003c-1.583-.001-3.14-.396-4.521-1.147l-5.016 1.31 1.342-4.88C.513 12.693.079 11.074.079 9.415.081 4.223 4.325-.001 9.539-.001c2.531.002 4.907.981 6.693 2.761C18.018 4.54 19.001 6.906 19 9.423c-.002 5.19-4.246 9.415-9.461 9.415zm5.563-14.954C13.618 2.405 11.643 1.59 9.543 1.59c-4.337 0-7.865 3.51-7.867 7.825 0 1.478.416 2.919 1.203 4.164l.186.297-.794 2.888 2.976-.777.288.169c1.207.713 2.591 1.091 4.001 1.092h.003c4.335 0 7.863-3.511 7.865-7.827 0-2.09-.817-4.057-2.302-5.537zm-2.53 10.014c-.407.061-.921.086-1.488-.093-.344-.109-.784-.253-1.348-.495-2.371-1.02-3.92-3.396-4.038-3.553-.118-.157-.967-1.276-.967-2.434 0-1.158.613-1.727.828-1.963.217-.235.474-.294.631-.294.158 0 .316.002.453.009.145.007.34-.055.532.404.197.471.671 1.628.729 1.747.059.117.098.255.02.411-.078.158-.118.255-.237.393-.118.137-.247.307-.355.411-.118.118-.241.246-.104.481.139.236.614 1.006 1.316 1.631.903.8 1.665 1.048 1.902 1.167.236.117.373.097.512-.059.137-.157.59-.687.748-.922.156-.236.315-.198.531-.118.218.078 1.38.647 1.616.765.235.118.394.176.453.275.059.098.059.569-.137 1.119-.199.549-1.143 1.051-1.597 1.118z" data-reactid="126"></path></svg>
        </a>
    </div> -->
</section>
<!-- <section class="social_block">
	<div class="container">
		<div class="social_block_content">
			<p>Будем друзьями ?</p>
			<ul>
				<li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i>нравится</a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i>читать</a></li>
				<li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i>вступить</a></li>
				<li><a href="#"><i class="fa fa-telegram" aria-hidden="true"></i>читать</a></li>
			</ul>
		</div>
	</div>
</section> -->
<section class="plus__news">
    <div class="container">
        <h3><?= Yii::t('frontend', 'Recent news')?></h3>
        <div class="more__news">
            <?php foreach ($lastArticles as $key=>$item) { ?>
            <?php if ($key == 0) {?>
            <div class="more__news__block">
                <a href="#">
                    <h4><?= $item->getMaterialNameLang()?></h4>
                    <p><?= date('d F Y', $item->created_at)?></p>
                </a>
            </div>
            <div class="more__news__block">
                <? } else {?>
                    <a href="<?= \yii\helpers\Url::toRoute(['/articles/view', 'slug' => $item->slug])?>" class="lot__news">
                        <h4><?= $item->getMaterialNameLang()?></h4>
                        <p><?= date('d F Y', $item->created_at)?></p>
                    </a>
                <? } ?>
                <? } ?></div>
        </div>
    </div>
</section>