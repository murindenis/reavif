<?php $this->title = 'REAVIF – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<div id="base-body">
<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <div class="container">
        <div class="help-banner">
            <span>«REAVIF» — <?= Yii::t('frontend', 'Fundacion - Readaptacion para la Vida')?></span>
            <p><?= $donate->getTitleLang()?></p>
        </div>
    </div>
    <hr class="hr-div">
    <section class="help-section">
        <div class="container">
            <div class="wrapper">
                <h2><?= Yii::t('frontend', 'Make a donation')?></h2>
                <p class="help-info"><?= $donate->getMakeDonationLang()?></p>
                <div class="tabs">
                    <div data-id="first" class="tab active"><?= Yii::t('frontend', 'Payment by card')?></div>
                    <div data-id="second" class="tab"><?= Yii::t('frontend', 'Bank transfer')?></div>
                </div>
                <div class="tabs-content">
                    <div id="first" class="content active">
                        <ul class="price">
                            <li><span class="price-item active">1 €</span></li>
                            <li><span class="price-item">2 €</span></li>
                            <li><span class="price-item">5 €</span></li>
                            <li><span class="price-item">10 €</span></li>
                            <li><span class="price-item">15 €</span></li>
                            <li><span class="price-item">20 €</span></li>
                            <li><span class="price-item">25 €</span></li>
                            <li><span class="price-item">50 €</span></li>
                            <li><span class="price-item">100 €</span></li>
                            <li><span class="price-item">200 €</span></li>
                            <li><input type="text" class="price-item" placeholder="<?= Yii::t('frontend', 'Another summ')?>"></span></li>
                        </ul>
                        <button class="btn btn-donate"><?= Yii::t('frontend', 'Donate')?></button>
                        <div class="checkbox_price">
                            <label>
                                <input class="checkbox hidden" type="checkbox" name="checkbox-test" checked>
                                <span class="checkbox-custom"></span>
                                <span class="checkbox-label">
                                    <?= Yii::t('frontend', 'I agree with')?>
                                     <a href="<?= \yii\helpers\Url::toRoute(['/donate/condition'])?>" target="_blank">
                                         <?= Yii::t('frontend', 'terms of use')?>
                                     </a>
                                </span>
                            </label>
                            <div class="pay_list">
                                <div class="pay_item"><img src="img/cards-1.png"></div>
                                <div class="pay_item"><img src="img/cards-2.png"></div>
                                <div class="pay_item"><img src="img/cards-3.png"></div>
                            </div>
                        </div>
                    </div>
                    <div id="second" class="content">
                        <div class="second_content">
                            <h3>«REAVIF» — <?= Yii::t('frontend', 'Fundacion - Readaptacion para la Vida')?></h3>
                            <div class="transfer">
                                <div class="transfer-item">
                                    <div class="transfer-title">
                                        <?= Yii::t('frontend', 'Address')?>
                                    </div>
                                    <div class="transfer-info">
                                        <?= $requisites->getAddressLang()?>
                                    </div>
                                </div>
                            </div>
                            <div class="transfer">
                                <div class="transfer-item">
                                    <div class="transfer-title">
                                        <?= Yii::t('frontend', 'CCC')?>
                                    </div>
                                    <div class="transfer-info">
                                        <?= $requisites->ccc?>
                                    </div>
                                </div>
                                <div class="transfer-item">
                                    <div class="transfer-title">
                                        <?= Yii::t('frontend', 'IBAN Electronic')?>
                                    </div>
                                    <div class="transfer-info">
                                        <?= $requisites->iban_electronic?>
                                    </div>
                                </div>
                                <div class="transfer-item">
                                    <div class="transfer-title">
                                        <?= Yii::t('frontend', 'IBAN Paper')?>
                                    </div>
                                    <div class="transfer-info">
                                        <?= $requisites->iban_paper?>
                                    </div>
                                </div>
                                <div class="transfer-item">
                                    <div class="transfer-title">
                                        <?= Yii::t('frontend', 'BIC')?>
                                    </div>
                                    <div class="transfer-info">
                                        <?= $requisites->bic?>
                                    </div>
                                </div>
                                <div class="transfer-item">
                                    <div class="transfer-title">
                                        <?= Yii::t('frontend', 'Phone')?>
                                    </div>
                                    <div class="transfer-info">
                                        <?= $requisites->phone?>
                                    </div>
                                </div>
                            </div>
                            <div class="transfer">
                                <div class="transfer-item">
                                    <div class="transfer-title">
                                        <?= Yii::t('frontend', 'Purpose of payments')?>
                                    </div>
                                    <div class="transfer-info">
                                        <?= Yii::t('frontend', 'A donation for the statutory activities of the Foundation for Assistance to people in REAVIF. Without VAT')?>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="logo_help">
                                <img src="http://res.cloudinary.com/dqxhdk0wi/image/upload/v1504876321/Shape_528_hvgy5r.svg">
                            </div> -->
                        </div>
                        <button class="btn btn-donate"><?= Yii::t('frontend', 'Download blank')?></button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
</div>