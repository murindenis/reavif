<?php $this->title = 'Reavif – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <section class="text__page condition__page">
        <div class="container">
            <div class="fund-wrapper">
                <div class="fund-content">
                    <?= $condition->getTextLang()?>
                </div>
                <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>
            </div>
        </div>
    </section>
</section>
