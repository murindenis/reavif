<?php $this->title = 'REAVIF – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<div id="second-body">
    <section class="help">
        <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
        <div class="container">
            <h1 class="fund_title"><?= Yii::t('frontend', 'About fund')?></h1>
        </div>
    </section>
    <section class="fund-section">
        <div class="container">
            <div class="fund-wrapper">
                <div class="fund-content">
                    <div class="tabs-title">
                        <div data-id="one" class="tab-fund active"><?= Yii::t('frontend', 'Who we are')?></div>
                        <!--<div data-id="two" class="tab-fund">Partners</div>-->
                        <div data-id="three" class="tab-fund"><?= Yii::t('frontend', 'Contacts')?></div>
                    </div>
                    <div class="tabs-content">
                        <div id="one" class="tabs-content__item active fund-text">
                            <h2><?= Yii::t('frontend', 'Fundacion REAVIF')?></h2>
                            <div class="tabs-info">
                                <?= $fund->getFundLang()?>
                            </div>
                            <!--<h2>The problem of society</h2>
                            <div class="tabs-info">
                                <?/*= $fund->getProblemLang()*/?>
                            </div>-->
                            <h2><?= Yii::t('frontend', 'Misión de organización')?></h2>
                            <div class="tabs-info">
                                <?= $fund->getMissionLang()?>
                            </div>
                            <h2><?= Yii::t('frontend', 'Our Objectives – principles of PRIMA0')?></h2>
                            <div class="tabs-info">
                                <?= $fund->getObjectivesLang()?>
                            </div>

                        </div>
                        <!--<div id="two" class="tabs-content__item">
                            <?php /*if (!empty($partners)) {*/?>
                            <h2>Partners of fund</h2>
                            <div class="tabs-info">
                                <p class="interview-text"><?/*= $fund->getPartnersLang()*/?></p>
                                <div class="partners_list">
                                    <?php /*foreach ($partners as $partner) {*/?>
                                    <div class="partners_item">
                                        <div class="interview-photo">
                                            <img src="<?/*= \common\behaviors\GlobalHelper::getImgPath($partner) */?>">
                                        </div>
                                        <div class="interview-author">
                                            <p class="author-name"><?/*= $partner->name*/?></p>
                                            <a href="<?/*= $partner->site_link*/?>"><?/*= $partner->site*/?></a>
                                        </div>
                                    </div>
                                    <?/* } */?>

                                </div>
                            </div>
                            <?/* } */?>
                        </div>-->
                        <div id="three" class="tabs-content__item">
                            <h2>Contacts</h2>
                            <div class="tabs-info">
                                <ul class="contact">
                                    <li>
                                        <span class="title_contact">Address</span>
                                        <span class="contact_info"><?= $contacts->getAddressLang()?></span>
                                    </li>
                                    <li>
                                        <span class="title_contact">Phone</span>
                                        <span class="contact_info"><a href="tel:+34630325115"><?= $contacts->phone?></a></span>
                                    </li>
                                    <li>
                                        <span class="title_contact">Email</span>
                                        <span class="contact_info contact_mail"><?= $contacts->email?></span>
                                    </li>
                                </ul>
                                <div class="maps">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2991.9202740364526!2d2.171818850468831!3d41.419249301881315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2cd65d00a09%3A0x366d4b4e71de25ef!2zQ2FycmVyIGRlIE1hc3BvbnMgaSBMYWJyw7NzLCAyOSwgMDgwNDEgQmFyY2Vsb25hLCDQmNGB0L_QsNC90LjRjw!5e0!3m2!1sru!2sby!4v1532445965742" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>
            </div>
        </div>
    </section>
</div>