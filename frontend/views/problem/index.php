<?php $this->title = 'REAVIF – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<div id="pages-body">
<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <div class="container">
        <h1 class="fund_title"><?= Yii::t('frontend', 'Relevancia')?></h1>
    </div>
    <section class="text__page" style="padding:0">
        <div class="container">
            <div class="fund-wrapper">
                <div class="fund-content">
                    <h2>The problem of society</h2>
                    <div class="tabs-info">
                        <?= $fund->getProblemLang()?>
                    </div>
                    <br/>
                    <?= $problem->getProblemTextLang()?>
                </div>
                <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>
            </div>
        </div>
    </section>
</section>
</div>