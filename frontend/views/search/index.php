<?php $this->title = 'REAVIF – FUNDACION – READAPTACION PARA LA VIDA'; ?>
<div id="loader-wrapper">
    <!--<div id="loader"></div>-->

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <div class="container">
        <div class="search__wrapper search__in__page">
            <form action="<?= \yii\helpers\Url::toRoute(['search/index'])?>">
                <input type="text" name="search" placeholder="<?= Yii::t('frontend', 'Search')?>" value="">
                <button>
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </form>
        </div>
    </div>
</section>
<section class="fund-section">
    <div class="container">
        <div class="fund-wrapper">
            <div class="fund-content">
                <span class="all__result">FOUND <?= $materialsCount?> MATCHES:</span>
                <div class="result__list">
                    <?php if (!empty($materials)) {?>
                        <?php foreach ($materials as $material) {?>
                        <div class="result__item" href="#">
                            <div href="#" class="result__content">
                                <a href="<?= $material->getMaterialUrl($material->category_id,$material->slug)?>" class="result__content__title"><p class="news-title"><span><?= $material->getMaterialCategoryNameLang()?> - </span><?= $material->getMaterialNameLang()?></p></a>
                                <ul class="news-hashtag">
                                    <?php foreach ($material->materialHashtag as $hashtag) { ?>
                                    <li><a href="<?= \yii\helpers\Url::toRoute(['/hashtag/index', 'slug' => $hashtag->hashtag->slug])?>"><?= $hashtag->hashtag->getHashtagNameLang()?></a></li>
                                    <? } ?>
                                </ul>
                            </div>
                            <a href="<?= $material->getMaterialUrl($material->category_id,$material->slug)?>" class="result__img">
                                <img src="<?= \common\behaviors\GlobalHelper::getImgPath($material)?>">
                            </a>
                        </div>
                        <? } ?>
                    <? } ?>
                    <!--<button type="button" class="btn btn-more">Show more</button>-->
                </div>
            </div>
            <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>

        </div>
    </div>
</section>