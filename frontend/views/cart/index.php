<?php
use yii\widgets\ActiveForm;
?>
<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <div class="container">
        <h1 class="fund_title"><?= Yii::t('frontend', 'Cart')?></h1>
    </div>
</section>
<section class="fund-section">
    <div class="container">
        <?php if (!empty($cart)) { ?>
        <div class="cart-box" data-locale="<?= Yii::t('frontend', 'Cart is empty')?>">
            <div class="basket__wrapper">
                <div class="table__with__product">
                    <div class="header__table__with__table">
                        <div class="title__photo__product"><?= Yii::t('frontend', 'Photo')?></div>
                        <div class="title__name__product"><?= Yii::t('frontend', 'Name')?></div>
                        <div class="title__price__product"><?= Yii::t('frontend', 'Price')?></div>
                        <div class="delete__product"></div>
                    </div>
                    <form id="cart-form-product">
                    <?php foreach ($cart as $item) {?>
                        <input type="hidden" name="cart[]" value="<?= $item->shopProduct->id?>">
                        <div class="item__product__in__table">
                            <div class="title__photo__product">
                                <img src="<?= \common\behaviors\GlobalHelper::getImgPath($item->shopProduct)?>">
                            </div>
                            <div class="title__name__product"><?= $item->shopProduct->getProductNameLang()?></div>
                            <div class="title__price__product title_pr_price"><?= $item->shopProduct->price?></div>
                            <div class="delete__product">
                                <i class="fa fa-times-circle drop-to-cart"
                                   data-id="<?= $item->id?>"
                                   data-text="<?= Yii::t('frontend', 'Product remove from Cart')?>"></i>
                            </div>
                        </div>
                    <? } ?>
                    </form>
                    <div class="total">
                        <div class="title__total">
                            <p><?= Yii::t('frontend', 'Total:')?></p>
                        </div>
                        <div class="value__total">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="more__info__about__delivery">
                <div class="more__info__about__delivery__list">
                    <!--<div class="table__item">
                        <div class="header__table__with__table">
                            <div class="title__table">Способ оплаты</div>
                        </div>
                        <div class="content__about__buyer">
                            <div class="checkbox__list">
                                <div class="checkbox__item">
                                    <label class="checkbox-1">
                                        <input type="radio" name="buyer">
                                        <span>checkbox-1</span>
                                    </label>
                                </div>
                                <div class="checkbox__item">
                                    <label class="checkbox-1">
                                        <input type="radio" name="buyer">
                                        <span>checkbox-1</span>
                                    </label>
                                </div>
                                <div class="checkbox__item">
                                    <label class="checkbox-1">
                                        <input type="radio" name="buyer">
                                        <span>checkbox-1</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table__item">
                        <div class="header__table__with__table">
                            <div class="title__table">Способ доставки</div>
                        </div>
                        <div class="content__about__buyer">
                            <div class="checkbox__list">
                                <div class="checkbox__item">
                                    <label class="checkbox-1">
                                        <input type="radio" name="delivery">
                                        <span>checkbox-1</span>
                                    </label>
                                </div>
                                <div class="checkbox__item">
                                    <label class="checkbox-1">
                                        <input type="radio" name="delivery">
                                        <span>checkbox-1</span>
                                    </label>
                                </div>
                                <div class="checkbox__item">
                                    <label class="checkbox-1">
                                        <input type="radio" name="delivery">
                                        <span>checkbox-1</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>-->
                </div>
                <div class="more__info__about__delivery__list">
                    <div class="header__table__with__table">
                        <div class="title__table"><?= Yii::t('frontend', 'Buyer')?></div>
                    </div>
                    <div class="content__about__buyer">
                        <?php $form = ActiveForm::begin(['options' => ['id' => 'cart-form-contact']]); ?>
                            <div class="form__item">
                                <label for="firstname"><?= Yii::t('frontend', 'Firstname')?></label>
                                <?php echo $form->field($user->userProfile, 'firstname')
                                    ->textInput()->label(false) ?>
                            </div>
                            <div class="form__item">
                                <label for="lastname"><?= Yii::t('frontend', 'Lastname')?></label>
                                <?php echo $form->field($user->userProfile, 'lastname')
                                    ->textInput()->label(false) ?>
                            </div>
                            <div class="form__item">
                                <label for="phone"><?= Yii::t('frontend', 'Phone')?></label>
                                <?php echo $form->field($user->userProfile, 'phone')
                                    ->textInput()->label(false) ?>
                            </div>
                            <div class="form__item">
                                <label for="country"><?= Yii::t('frontend', 'Country')?></label>
                                <?php echo $form->field($user->userProfile, 'country')
                                    ->textInput()->label(false) ?>
                            </div>
                            <div class="form__item">
                                <label for="city"><?= Yii::t('frontend', 'City')?></label>
                                <?php echo $form->field($user->userProfile, 'city')
                                    ->textInput()->label(false) ?>
                            </div>
                            <div class="form__item">
                                <label for="street"><?= Yii::t('frontend', 'Street')?></label>
                                <?php echo $form->field($user->userProfile, 'street')
                                    ->textInput()->label(false) ?>
                            </div>
                            <div class="form__item">
                                <label for="house"><?= Yii::t('frontend', 'House')?></label>
                                <?php echo $form->field($user->userProfile, 'house')
                                    ->textInput()->label(false) ?>
                            </div>
                            <div class="form__item">
                                <label for="apartment"><?= Yii::t('frontend', 'Apartment')?></label>
                                <?php echo $form->field($user->userProfile, 'apartment')
                                    ->textInput()->label(false) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <div class="button__buy">
                <a class="btn btn-donate btn-donate--header btn-order" data-text="<?= Yii::t('frontend','Order Completed')?>"><?= Yii::t('frontend', 'Buy')?></a>
            </div>
        </div>
        <? } else {?>
            <?= Yii::t('frontend', 'Cart is empty')?>
        <? } ?>
    </div>
</section>