<?php

namespace frontend\modules\user\controllers;

use backend\models\search\ShopOrdersSearch;
use common\base\MultiModel;
use common\models\FileStorageItem;
use common\models\Material;
use common\models\MaterialHashtag;
use common\models\ShopOrders;
use common\models\ShopOrdersProduct;
use frontend\modules\user\models\AccountForm;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;

class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read());
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $accountForm = new AccountForm();
        $accountForm->setUser(Yii::$app->user->identity);

        $model = new MultiModel([
            'models' => [
                'account' => $accountForm,
                'profile' => Yii::$app->user->identity->userProfile
            ]
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $locale = $model->getModel('profile')->locale;
            Yii::$app->session->setFlash('forceUpdateLocale');
            Yii::$app->session->setFlash('alert', [
                'options' => ['class' => 'alert-success'],
                'body' => Yii::t('frontend', 'Your account has been successfully saved', [], $locale)
            ]);
            return $this->refresh();
        }

        $shopOrders = ShopOrders::find()->select('id')->where(['user_id' => Yii::$app->user->id])->all();

        $shopOrdersIds = [];
        foreach ($shopOrders as $shopOrder) {
            $shopOrdersIds[] = $shopOrder['id'];
        }
        $shopOrdersProducts = ShopOrdersProduct::find()->where(['shop_orders_id' => $shopOrdersIds])->all();

        $materials = Material::find()->where(['user_id' => Yii::$app->user->id])->all();

        return $this->render('index', [
            'model' => $model,
            'shopOrdersProducts' => $shopOrdersProducts,
            'materials' => $materials,
        ]);
    }

    public function actionCreateMaterial() {

        $model = new Material();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $hashtagArray = Yii::$app->request->post()['Material']['hashtags'];
            if (!empty($hashtagArray)) {
                foreach ($hashtagArray as $hashtag) {
                    $newMaterialHashtag = new MaterialHashtag();
                    $newMaterialHashtag->material_id = $model->id;
                    $newMaterialHashtag->hashtag_id  = $hashtag;
                    $newMaterialHashtag->save();
                }
            }
            $this->savePhoto($model);
            return $this->redirect(['/user/default/index']);
        }

        return $this->render('create-material',[
            'model' => $model,
        ]);
    }

    public function savePhoto($material)
    {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/material/' . $material->id;
        if (count($material->path) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }

            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $material->path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $material->path);
                //Копируем фотографии из временной папки 1 в папку с id квартиры
                if (($photoPath['0'] == '1') || ($photoPath['0'] == $material->id)) {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    //Изменяем записи в БД об изображениии
                    if ($copyStatus) {
                        $oldPath = $material->path;
                        $material->updateAttributes(['path' => 'material/' . $material->id . '/' . $photoPath['1']]);
                        //$material->path = 'material/' . $material->id . '/' . $photoPath['1'];
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'material/' . $material->id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }

        }
    }
}
