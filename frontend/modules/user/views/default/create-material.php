<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;
?>
<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
</section>
<section class="fund-section">
    <div class="container">
        <section class="article">

            <div class="container">
                <?php $form = ActiveForm::begin(); ?>

                <?//= $form->field($model, 'category_id')->dropDownList($model->getCategoryColumn(), ['prompt' => '-']) ?>
                <?= $form->field($model, 'category_id')->hiddenInput(['value' => \common\models\Material::MATERIAL_NEWS])->label(false)?>
                <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false)?>
                <?= $form->field($model, 'hashtags')->widget(\dosamigos\multiselect\MultiSelect::className(),[
                    'data' => $model->getHashtagColumn(),
                    "options" => ['multiple'=>"multiple"],
                    "clientOptions" =>
                        [
                            'enableFiltering' => true,
                        ],
                ]) ?>

                <?= $form->field($model, 'title_eng')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'title_esp')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'hashtag_id')->dropDownList($model->getHashtagColumn(), ['prompt' => '-']) ?>

                <?= $form->field($model, 'introtext_eng')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'introtext_esp')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'description_eng')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                        'options'=> [
                            'minHeight'   => 300,
                            'maxHeight'   => 600,
                            'buttonSource'=> true,
                            'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                ) ?>

                <?= $form->field($model, 'description_esp')->widget(
                    \yii\imperavi\Widget::className(),
                    [
                        'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                        'options'=> [
                            'minHeight'   => 300,
                            'maxHeight'   => 600,
                            'buttonSource'=> true,
                            'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                        ]
                    ]
                ) ?>

                <?php echo $form->field($model, 'picture')->widget(
                    \trntv\filekit\widget\Upload::classname(),
                    [
                        'url' => ['avatar-upload']
                    ]
                ) ?>

                <?= $form->field($model, 'status')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </section>
    </div>
</section>