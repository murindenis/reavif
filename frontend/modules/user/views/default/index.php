<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\base\MultiModel */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="help">
    <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');?>
    <div class="container">
        <h1 class="fund_title"><?= Yii::t('frontend', 'User`s Cabinet')?></h1>
    </div>
</section>

    <section class="fund-section">
        <div class="container">
            <div class="fund-wrapper">
                <div class="fund-content">
                    <div class="tabs-title">
                        <div data-id="one" class="tab-fund active"><?= Yii::t('frontend', 'My Donations')?></div>
                        <!--<div data-id="two" class="tab-fund"><?/*= Yii::t('frontend', 'My Orders')*/?></div>-->
                        <?php if (Yii::$app->user->can('specialist')) { ?>
                        <div data-id="three" class="tab-fund"><?= Yii::t('frontend', 'My Articles')?></div>
                        <? } ?>
                        <div data-id="four" class="tab-fund"><?= Yii::t('frontend', 'My Profile')?></div>
                    </div>
                    <div class="tabs-content">
                        <div id="one" class="tabs-content__item active">
                            <div class="table__donations">
                                <div class="header__table__donations">
                                    <div class="cell__item">
                                        <span>Дата</span>
                                    </div>
                                    <div class="cell__item">
                                        <span>Сумма пожертвования</span>
                                    </div>
                                </div>
                                <div class="content__table__donations">
                                    <div class="cell__item">
                                        <span>202020</span>
                                    </div>
                                    <div class="cell__item">
                                        <span>202020</span>
                                    </div>
                                </div>
                                <div class="content__table__donations">
                                    <div class="cell__item">
                                        <span>202020</span>
                                    </div>
                                    <div class="cell__item">
                                        <span>202020</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div id="two" class="tabs-content__item">
                            <div class="table__donations specialist">
                                <div class="header__table__donations">
                                    <div class="cell__item">
                                        <span><?/*= Yii::t('frontend', 'Date')*/?></span>
                                    </div>
                                    <div class="cell__item">
                                        <span><?/*= Yii::t('frontend', 'Photo')*/?></span>
                                    </div>
                                    <div class="cell__item">
                                        <span><?/*= Yii::t('frontend', 'Product')*/?></span>
                                    </div>
                                    <div class="cell__item">
                                        <span><?/*= Yii::t('frontend', 'Price')*/?>, EUR</span>
                                    </div>
                                </div>
                                <?php /*if (!empty($shopOrdersProducts)) {*/?>
                                    <?php /*foreach ($shopOrdersProducts as $shopOrdersProduct) { */?>
                                        <div class="header__table__donations">
                                            <div class="cell__item">
                                                <span><?/*= date('d M Y', $shopOrdersProduct->shopOrders->created_at)*/?></span>
                                            </div>
                                            <div class="cell__item">
                                                <span>
                                                    <img src="<?/*= \common\behaviors\GlobalHelper::getImgPath($shopOrdersProduct->shopProduct)*/?>" width="100%">
                                                </span>
                                            </div>
                                            <div class="cell__item">
                                                <span><?/*= $shopOrdersProduct->shopProduct->getProductNameLang() */?></span>
                                            </div>
                                            <div class="cell__item">
                                                <span class="red"><?/*= $shopOrdersProduct->shopProduct->price */?></span>
                                            </div>
                                        </div>
                                    <?/* }*/?>
                                <?/* } else {*/?>
                                    <p><?/*= Yii::t('frontend', 'No Product')*/?></p>
                                <?/* }*/?>
                            </div>
                        </div>-->
                        <?php if (Yii::$app->user->can('specialist')) { ?>
                            <div id="three" class="tabs-content__item">
                                <div class="table__donations specialist">
                                    <div class="header__table__donations">
                                        <div class="cell__item">
                                            <span><?= Yii::t('frontend', 'Date')?></span>
                                        </div>
                                        <div class="cell__item">
                                            <span><?= Yii::t('frontend', 'Name')?></span>
                                        </div>
                                        <div class="cell__item">
                                            <span><?= Yii::t('frontend', 'Active')?></span>
                                        </div>
                                    </div>
                                    <?php if ($materials) {?>
                                        <?php foreach ($materials as $material) { ?>
                                        <div class="content__table__donations">
                                            <div class="cell__item">
                                                <span><?= date('d M Y', $material->created_at)?></span>
                                            </div>
                                            <div class="cell__item">
                                                <span><?= $material->getMaterialNameLang()?></span>
                                            </div>
                                            <div class="cell__item">
                                                <span class="<?= $material->status ? 'green' : 'red'?>"><?= $material->getMaterialStatus()?></span>
                                            </div>
                                        </div>
                                        <? } ?>
                                    <? } else {?>
                                        <p><?= Yii::t('frontend', 'No Articles')?></p>
                                    <? } ?>
                                    <div class="add__info">
                                        <a class="btn btn-donate btn-donate--header" href="<?= \yii\helpers\Url::toRoute(['/user/default/create-material'])?>"><?= Yii::t('frontend', 'Add Article')?></a>
                                    </div>
                                </div>
                            </div>
                        <? } ?>
                        <div id="four" class="tabs-content__item">
                            <?php  echo $this->render('_profile', ['model' => $model]); ?>
                        </div>
                    </div>
                </div>
                <?php require_once(Yii::getAlias('@frontend').'/views/layouts/section/baner-block.php');?>
            </div>
        </div>
    </section>