<?php
use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(); ?>

    <h2><?php echo Yii::t('frontend', 'Profile settings') ?></h2>

<?php echo $form->field($model->getModel('profile'), 'picture')->widget(
    Upload::classname(),
    [
        'url' => ['avatar-upload']
    ]
)?>
    <div class="my__profil">
        <label>
            <?= Yii::t('frontend', 'Firstname')?>
            <?php echo $form->field($model->getModel('profile'), 'firstname')
                ->textInput()->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'Lastname')?>
            <?php echo $form->field($model->getModel('profile'), 'lastname')
                ->textInput()->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'Phone')?>
            <?php echo $form->field($model->getModel('profile'), 'phone')
                ->textInput()->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'Country')?>
            <?php echo $form->field($model->getModel('profile'), 'country')
                ->textInput()->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'City')?>
            <?php echo $form->field($model->getModel('profile'), 'city')
                ->textInput()->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'Street')?>
            <?php echo $form->field($model->getModel('profile'), 'street')
                ->textInput()->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'House')?>
            <?php echo $form->field($model->getModel('profile'), 'house')
                ->textInput()->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'Apartment')?>
            <?php echo $form->field($model->getModel('profile'), 'apartment')
                ->textInput()->label(false) ?>
        </label>

        <?/*= Yii::t('frontend', 'Locale')*/?><!--
            --><?php /*echo $form->field($model->getModel('profile'), 'locale')
                ->dropDownlist(Yii::$app->params['availableLocales'])->label(false) */?>
    </div>

    <h2><?php echo Yii::t('frontend', 'Account Settings') ?></h2>
    <div class="my__profil">
        <label>
            <?= Yii::t('frontend', 'Username')?>
            <?php echo $form->field($model->getModel('account'), 'username')->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'E-mail')?>
            <?php echo $form->field($model->getModel('account'), 'email')->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'Password')?>
            <?php echo $form->field($model->getModel('account'), 'password')->label(false) ?>
        </label>
        <label>
            <?= Yii::t('frontend', 'Password confirm')?>
            <?php echo $form->field($model->getModel('account'), 'password_confirm')->label(false) ?>
        </label>
    </div>
    <div class="my__profil__button">
        <?php echo Html::submitButton(Yii::t('frontend', 'Save'), ['class' => 'btn btn-donate btn-donate--header']) ?>
    </div>

<?php ActiveForm::end(); ?>