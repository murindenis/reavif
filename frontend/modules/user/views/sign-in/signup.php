<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\SignupForm */
$this->title = 'Reavif – FUNDACION – READAPTACION PARA LA VIDA';
/*$this->title = Yii::t('frontend', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
*/?><!--
<div class="site-signup">
    <h1><?php /*echo Html::encode($this->title) */?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php /*$form = ActiveForm::begin(['id' => 'form-signup']); */?>
                <?php /*echo $form->field($model, 'username') */?>
                <?php /*echo $form->field($model, 'email') */?>
                <?php /*echo $form->field($model, 'password')->passwordInput() */?>
                <div class="form-group">
                    <?php /*echo Html::submitButton(Yii::t('frontend', 'Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) */?>
                </div>
                <h2><?php /*echo Yii::t('frontend', 'Sign up with')  */?>:</h2>
                <div class="form-group">
                    <?php /*echo yii\authclient\widgets\AuthChoice::widget([
                        'baseAuthUrl' => ['/user/sign-in/oauth']
                    ]) */?>
                </div>
            <?php /*ActiveForm::end(); */?>
        </div>
    </div>
</div>-->

<div class="regisrtation">
    <div class="wrapper__authorization">
        <h1><?= Yii::t('frontend','Registration')?></h1>
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <div class="wrapper__input__authorization">
                <?php echo $form->field($model, 'username')->textInput(['class' => 'input__registration', 'placeholder' => Yii::t('frontend','Username')])->label(false) ?>
                <?php echo $form->field($model, 'email')->textInput(['class' => 'input__registration', 'placeholder' => 'E-mail'])->label(false) ?>
                <?php echo $form->field($model, 'password')->passwordInput(['class' => 'input__registration', 'placeholder' => Yii::t('frontend','Password')])->label(false) ?>
            </div>
            <div class="button__authorization">
                <?php echo Html::submitButton('Register', ['name' => 'signup-button', 'class' => 'btn btn-donate btn-donate--header']) ?>
            </div>
            <!--<div class="more__function__authorization">
                <label class="checkbox-1">
                    <input type="checkbox" name="delivery">
                    <span><?/*= Yii::t('frontend', 'I accept the terms of the user agreement')*/?></span>
                </label>
            </div>-->
        <?php ActiveForm::end(); ?>
    </div>
    <a href="/" class="close__authorisation">
        <i class="fa fa-times"></i>
    </a>
</div>