<?php

namespace frontend\assets;

use common\assets\Html5shiv;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Reavif application asset
 */
class ReavifAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $css = [
        'reavif/css/vendor.css',
        'reavif/css/style.css',
        'reavif/css/fm.revealator.jquery.css',
    ];

    public $js = [
        'reavif/js/vendor.js',
        'reavif/js/custom.js',
        'reavif/js/fm.revealator.jquery.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        YiiAsset::class,
        //BootstrapAsset::class,
        Html5shiv::class,
    ];
}
