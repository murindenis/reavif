'use strict';
$(document).ready(function () {
    setTimeout(function(){
        $('body').addClass('loaded');
    }, 1000);
	$('.slider-block').slick({
		dots: true,
		prevArrow: $('.prev_arrows'),
		nextArrow: $('.next_arrows')
	});
	$('.slider-words-block').slick({
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,
		autoplayTimeout: 0,
		dots: false,
		slidesToScroll: 1,
		swipeToSlide: true,
		responsive: [{
			breakpoint: 3000,
			settings: {
				slidesToShow: 10
			}
		}, {
			breakpoint: 1500,
			settings: {
				slidesToShow: 7
			}
		}, {
			breakpoint: 1024,
			settings: {
				slidesToShow: 5
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 480,
			settings: {
				slidesToShow: 1
			}
		}]
	});

	$('header').on('mouseover', function () {
		$('.slider-section').addClass('active');
	});
	$('header').on('mouseout', function () {
		$('.slider-section').removeClass('active');
	});

	$('.tab').on('click', function () {
		var id = $(this).data('id');
		var content = $('#' + id);
		$(this).siblings('.tab').removeClass('active');
		$(this).addClass('active');
		content.siblings('.content').removeClass('active');
		content.addClass('active');
	});

	$('.price-item').on('click', function () {
		$(this).closest('.price').find('.price-item').removeClass('active');
		$(this).addClass('active');
	});

	$('.tab-fund').on('click', function () {
		var id = $(this).data('id');
		var content = $('#' + id);
		$(this).siblings('.tab-fund').removeClass('active');
		$(this).addClass('active');
		content.siblings('.tabs-content__item').removeClass('active');
		content.addClass('active');
	});

	var hamburgerMenu = function () {
		return {
			init: function init() {
				var btnHamburger = $('.btn-hamburger'),
					nav = $('.navigation'),
					overlay = $('.nav__overlay'),
					closeBtn = $('.btn-close');

				btnHamburger.on('click', function () {
					nav.addClass('active');
					overlay.addClass('active');
				});

				closeBtn.on('click', function () {
					nav.removeClass('active');
					overlay.removeClass('active');
				});

				// if (document.querySelector('html').offsetWidth < 768) {
				// 	btn.on('click', function () {
				// 		nav.addClass('active');
				// 		overlay.addClass('active');
				// 	});

				// 	closeBtn.on('click', function () {
				// 		nav.removeClass('active');
				// 		overlay.removeClass('active');
				// 	});

				// 	overlay.on('click', function () {
				// 		nav.removeClass('active');
				// 		overlay.removeClass('active');
				// 	});
				// }
			}
		};
	}();
	hamburgerMenu.init();

	$(document).ready(function () {
		$('select').niceSelect();

		setTimeout(function () {
			$('.pop-up').addClass('active');
			$('.fa-times').on('click', function () {
				$('.pop-up').removeClass('active');
			});
		}, 5000);

        var form = $('#subscribeForm');

        form.on('submit', function (e) {
            e.preventDefault();

            var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
            if($('.email-input').val() != '') {
                if ($('.email-input').val().search(pattern) == 0) {
                    $.ajax({
                        type: 'post',
                        data: form.serialize(),
                        url: '/site/email-ajax',
                        success: function (data) {
                            alert('Success!');
                            form.trigger('reset')
                        },
                    });
                } else {
                	alert('Incorrect Email');
				}
            } else {
            	alert('Select Email');
			}
        });
        // cart
        var addToCart = $('.add-to-cart');
        var dropToCart = $('.drop-to-cart');
        var cartBox = $('.cart-box');

        function Notification($text) {
            $('.notification').html($text).show();
            setTimeout(function () {
                $('.notification').hide();
            }, 3000);
        }

        addToCart.on('click', function (e) {
            e.preventDefault();
            var el = $(this);

            $.ajax({
                type: 'post',
				data: {user_id: el.data('user_id'), product_id: el.data('product_id')},
                url: '/shop/add-to-cart-ajax',
                success: function (data) {
                    Notification(el.data('text'));
                },
            });
        });
        dropToCart.on('click', function (e) {
        	e.preventDefault();
            var el = $(this);
            $.ajax({
                type: 'post',
                data: {id: el.data('id')},
                url: '/shop/drop-from-cart-ajax',
                success: function (data) {
                    Notification(el.data('text'));
                    el.closest('.item__product__in__table').prev().remove();
                    el.closest('.item__product__in__table').remove();
                    totalPrice();
                    var trigger = $('.cart-box').find('.item__product__in__table');

                    if(!trigger.length) {
                        $('.cart-box').remove();
                        $('.fund-section .container').html(cartBox.data('locale'));
					}

                },
            });
		});

        // Подсчет общей суммы заказа
        function totalPrice() {
            var sum = 0;
            $('.title_pr_price').each(function(indx, element){
                var this_p = $(this);
                var text = $(this_p).text();
                sum += +text;
            });
            $('.value__total p').html(sum+'EUR');
        }
        totalPrice();

        // new order
		var orderBtn = $('.btn-order');
		var cartProductForm = $('#cart-form-product');
		var cartContactForm = $('#cart-form-contact');

        orderBtn.on('click', function (e) {
            e.preventDefault();
            var el = $(this);
            var dataProduct = cartProductForm.serialize();
            $.ajax({
                type: 'post',
                data: dataProduct,
                url: '/shop/order-product-ajax',
                success: function (data) {
                    $.ajax({
                        type: 'post',
                        data: cartContactForm.serialize(),
                        url: '/shop/order-contact-ajax',
                        success: function (data) {
                            Notification(el.data('text'));
                        },
                    });

					$('.cart-box').remove();
					$('.fund-section .container').html(cartBox.data('locale'));

                },
            });
        });
	});

	$('.dela__fonda').slick({
		dots: true,
		infinite: true,
		speed: 300,
		arrows: false,
		slidesToShow: 1,
		adaptiveHeight: true
	});


		svg4everybody({});

	$('.open__pop-up').on('click', function () {
		$('.search__pop-up').addClass('active');
		$('body').css('overflow', 'hidden');
	});
	$('.close__pop-up').on('click', function () {
		$('.search__pop-up').removeClass('active');
		$('body').css('overflow', 'auto');
	});

});