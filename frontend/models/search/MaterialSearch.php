<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Material;

/**
 * MaterialSearch represents the model behind the search form of `common\models\Material`.
 */
class MaterialSearch extends Material
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'hashtag_id'], 'integer'],
            [['title_eng', 'title_esp', 'slug', 'introtext_eng', 'introtext_esp', 'description_eng', 'description_esp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Material::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'month',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'hashtag_id' => $this->hashtag_id,
            'created_at' => $this->created_at,
            ]);

        $query->andFilterWhere(['like', 'title_eng', $this->title_eng])
            ->andFilterWhere(['like', 'title_esp', $this->title_esp])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'introtext_eng', $this->introtext_eng])
            ->andFilterWhere(['like', 'introtext_esp', $this->introtext_esp])
            ->andFilterWhere(['like', 'description_eng', $this->description_eng])
            ->andFilterWhere(['like', 'description_esp', $this->description_esp]);

        return $dataProvider;
    }
}
