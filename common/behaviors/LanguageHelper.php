<?php
namespace common\behaviors;

class LanguageHelper {
    const LANG_ENG = 'en-US';
    const LANG_ESP = 'es';
}