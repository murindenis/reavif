<?php
namespace common\behaviors;

class GlobalHelper {

    public static function getImgPath($model) {
        if ($model->base_url && $model->path) {
            return $model->base_url.'/'.$model->path;
        } else {
            return '/img/no-image.svg';
        }
    }
}