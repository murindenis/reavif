<?php

use yii\db\Migration;

/**
 * Handles adding phone_and_about to table `user_profile`.
 */
class m180227_140509_add_phone_and_about_columns_to_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user_profile', 'about', $this->text());
        $this->addColumn('user_profile', 'phone', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user_profile','about');
        $this->dropColumn('user_profile','phone');
    }
}
