<?php

use yii\db\Schema;
use common\rbac\Migration;
use common\models\User;

class m180227_133928_add_specialist_role_to_rbac_tables extends Migration
{
    public function up()
    {
        $specialist = $this->auth->createRole(User::ROLE_SPECIALIST);
        $this->auth->add($specialist);
        $this->auth->addChild($this->auth->getRole(User::ROLE_ADMINISTRATOR), $specialist);
        $this->auth->addChild($this->auth->getRole(User::ROLE_MANAGER), $specialist);
    }

    public function down()
    {
        $this->auth->remove($this->auth->getRole(User::ROLE_SPECIALIST));
    }
}
