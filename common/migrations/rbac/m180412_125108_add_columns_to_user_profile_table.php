<?php

use yii\db\Migration;

class m180412_125108_add_columns_to_user_profile_table extends Migration
{
    public function up()
    {
        $this->addColumn('user_profile', 'country', $this->string());
        $this->addColumn('user_profile', 'city', $this->string());
        $this->addColumn('user_profile', 'street', $this->string());
        $this->addColumn('user_profile', 'house', $this->string());
        $this->addColumn('user_profile', 'apartment', $this->string());
    }

    public function down()
    {
        $this->dropColumn('user_profile','apartment');
        $this->dropColumn('user_profile','house');
        $this->dropColumn('user_profile','street');
        $this->dropColumn('user_profile','city');
        $this->dropColumn('user_profile','country');
    }
}
