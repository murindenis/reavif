<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_photo`.
 */
class m180227_103501_create_material_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('material_photo', [
            'id'         => $this->primaryKey(),
            'material_id'    => $this->integer()->notNull(),
            'path'       => $this->string(1024)->notNull(),
            'base_url'   => $this->string(),
            'type'       => $this->string(),
            'size'       => $this->integer(),
            'name'       => $this->string(),
            'order'      => $this->integer(),
            'created_at' => $this->integer(),

        ], $tableOptions);

        $this->createIndex('fk_material_photo_material', '{{%material_photo}}', 'material_id');
        $this->addForeignKey('idx_material_photo_material_id', '{{%material_photo}}', 'material_id', '{{%material}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_material_photo_material_id','material_photo');
        $this->dropIndex('idx_material_photo_material','material_photo');

        $this->dropTable('material_photo');
    }
}
