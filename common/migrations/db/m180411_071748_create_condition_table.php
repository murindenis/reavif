<?php

use yii\db\Migration;

/**
 * Handles the creation of table `condition`.
 */
class m180411_071748_create_condition_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('condition', [
            'id' => $this->primaryKey(),
            'text_eng' => $this->text(),
            'text_esp' => $this->text(),
        ], $tableOptions);

        $this->insert('condition', [
            'text_eng' => '1',
            'text_esp' => '1',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('condition');
    }
}
