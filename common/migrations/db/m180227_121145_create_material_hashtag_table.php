<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_hashtag`.
 */
class m180227_121145_create_material_hashtag_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('material_hashtag', [
            'id' => $this->primaryKey(),
            'material_id' => $this->integer()->notNull(),
            'hashtag_id'  => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-material_hashtag_material', '{{%material_hashtag}}', 'material_id');
        $this->addForeignKey('fk-material_hashtag_material_id', '{{%material_hashtag}}', 'material_id', '{{%material}}', 'id', 'cascade', 'cascade');

        $this->createIndex('idx-material_hashtag_hashtag', '{{%material_hashtag}}', 'hashtag_id');
        $this->addForeignKey('fk-material_hashtag_hashtag_id', '{{%material_hashtag}}', 'hashtag_id', '{{%hashtag}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-material_hashtag_hashtag_id','material_hashtag');
        $this->dropIndex('idx-material_hashtag_hashtag','material_hashtag');

        $this->dropForeignKey('fk-material_hashtag_material_id','material_hashtag');
        $this->dropIndex('idx-material_hashtag_material','material_hashtag');

        $this->dropTable('material_hashtag');
    }
}
