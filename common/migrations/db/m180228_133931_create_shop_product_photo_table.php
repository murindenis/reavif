<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_product_photo`.
 */
class m180228_133931_create_shop_product_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('shop_product_photo', [
            'id'              => $this->primaryKey(),
            'shop_product_id' => $this->integer()->notNull(),
            'path'            => $this->string(1024)->notNull(),
            'base_url'        => $this->string(),
            'type'            => $this->string(),
            'size'            => $this->integer(),
            'name'            => $this->string(),
            'order'           => $this->integer(),
            'created_at'      => $this->integer(),
        ],$tableOptions);

        $this->createIndex('idx-shop_product_photo_shop_product', '{{%shop_product_photo}}', 'shop_product_id');
        $this->addForeignKey('fk-shop_product_photo_shop_product_id', '{{%shop_product_photo}}', 'shop_product_id', '{{%shop_product}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-shop_product_photo_shop_product_id','shop_product_photo');
        $this->dropIndex('idx-shop_product_photo_shop_product','shop_product_photo');

        $this->dropTable('shop_product_photo');
    }
}
