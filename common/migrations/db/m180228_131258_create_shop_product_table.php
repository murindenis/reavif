<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_product`.
 */
class m180228_131258_create_shop_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('shop_product', [
            'id'              => $this->primaryKey(),
            'name_eng'        => $this->string()->notNull(),
            'name_esp'        => $this->string()->notNull(),
            'description_eng' => $this->string(),
            'description_esp' => $this->string(),
            'price'           => $this->integer()->notNull(),
            'created_at'      => $this->integer(),
        ],$tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shop_product');
    }
}
