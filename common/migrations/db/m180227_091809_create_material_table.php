<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material`.
 */
class m180227_091809_create_material_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('material', [
            'id'              => $this->primaryKey(),
            'category_id'     => $this->integer()->notNull(),
            'title_eng'       => $this->string()->notNull(),
            'title_esp'       => $this->string()->notNull(),
            'slug'            => $this->string()->notNull(),
            'hashtag_id'      => $this->integer()->notNull(),
            'introtext_eng'   => $this->string(),
            'introtext_esp'   => $this->string(),
            'description_eng' => $this->text(),
            'description_esp' => $this->text(),
            'created_at'      => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-material_category', '{{%material}}', 'category_id');
        $this->addForeignKey('fk-material_category_id', '{{%material}}', 'category_id', '{{%category}}', 'id', 'cascade', 'cascade');

        $this->createIndex('idx-material_hashtag', '{{%material}}', 'hashtag_id');
        $this->addForeignKey('fk-material_hashtag_id', '{{%material}}', 'hashtag_id', '{{%hashtag}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-material_hashtag_id', 'material');
        $this->dropIndex('fk-material_hashtag', 'material');

        $this->dropForeignKey('fk-material_category_id', 'category');
        $this->dropIndex('fk-material_category', 'category');

        $this->dropTable('material');
    }
}
