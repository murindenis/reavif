<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_profession`.
 */
class m180227_131444_create_user_profession_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('user_profession', [
            'id'         => $this->primaryKey(),
            'name_eng'   => $this->string()->notNull(),
            'name_esp'   => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_profession');
    }
}
