<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m180227_125023_create_slider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('slider', [
            'id'          => $this->primaryKey(),
            'material_id' => $this->integer()->notNull(),
            'created_at'  => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-slider_material', '{{%slider}}', 'material_id');
        $this->addForeignKey('fk-slider_material_id', '{{%slider}}', 'material_id', '{{%material}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('slider_material_id','slider');
        $this->dropIndex('slider_material','slider');

        $this->dropTable('slider');
    }
}
