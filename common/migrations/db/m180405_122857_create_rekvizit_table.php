<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rekvizit`.
 */
class m180405_122857_create_rekvizit_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('rekvizit', [
            'id' => $this->primaryKey(),
            'address_eng' => $this->string(),
            'address_esp' => $this->string(),
            'ccc' => $this->string(),
            'iban_electronic' => $this->string(),
            'iban_paper' => $this->string(),
            'bic' => $this->string(),
            'phone' => $this->string(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rekvizit');
    }
}
