<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fund`.
 */
class m180405_105334_create_fund_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('fund', [
            'id' => $this->primaryKey(),
            'fund_eng' => $this->text(),
            'fund_esp' => $this->text(),
            'problem_eng' =>$this->text(),
            'problem_esp' =>$this->text(),
            'mission_eng' =>$this->text(),
            'mission_esp' =>$this->text(),
            'objectives_eng' =>$this->text(),
            'objectives_esp' =>$this->text(),
            'partners_eng' =>$this->text(),
            'partners_esp' =>$this->text(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('fund');
    }
}
