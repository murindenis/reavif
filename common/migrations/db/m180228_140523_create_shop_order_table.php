<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180228_140523_create_shop_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('shop_order', [
            'id'         => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name'  => $this->string()->notNull(),
            'email'      => $this->string()->notNull(),
            'phone'      => $this->string()->notNull(),
            'zip'        => $this->string()->notNull(),
            'country'    => $this->string()->notNull(),
            'city'       => $this->string()->notNull(),
            'street'     => $this->string()->notNull(),
            'house'      => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ],$tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shop_order');
    }
}
