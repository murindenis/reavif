<?php

use yii\db\Migration;

/**
 * Handles the creation of table `social`.
 */
class m180623_083739_create_social_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('social', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'link' => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ],$tableOptions);

        $this->insert('social', ['name' => 'facebook', 'link' => '']);
        $this->insert('social', ['name' => 'twitter', 'link' => '']);
        $this->insert('social', ['name' => 'vk', 'link' => '']);
        $this->insert('social', ['name' => 'instagram', 'link' => '']);
        $this->insert('social', ['name' => 'youtube', 'link' => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('social');
    }
}
