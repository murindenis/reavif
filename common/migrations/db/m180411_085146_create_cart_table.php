<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cart`.
 */
class m180411_085146_create_cart_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('cart', [
            'id' => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'shop_product_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ],$tableOptions);

        $this->createIndex('idx-cart_user_id', '{{%cart}}', 'user_id');
        $this->addForeignKey('fk-cart_user_id', '{{%cart}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');

        $this->createIndex('idx-cart_shop_product_id', '{{%cart}}', 'shop_product_id');
        $this->addForeignKey('fk-cart_shop_product_id', '{{%cart}}', 'shop_product_id', '{{%shop_product}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('cart','fk-cart_shop_product_id');
        $this->dropIndex('cart', 'idx-cart_shop_product_id');
        $this->dropForeignKey('cart','fk-cart_user_id');
        $this->dropIndex('cart', 'idx-cart_user_id');

        $this->dropTable('cart');
    }
}
