<?php

use yii\db\Migration;

/**
 * Handles the creation of table `problem`.
 */
class m180623_164509_create_problem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('problem', [
            'id' => $this->primaryKey(),
            'text_eng' => $this->text(),
            'text_esp' => $this->text(),

        ], $tableOptions);

        $this->insert('problem', ['text_eng' => 'text problem eng', 'text_esp' => 'text problem esp']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('problem');
    }
}
