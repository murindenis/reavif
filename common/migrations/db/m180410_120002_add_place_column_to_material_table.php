<?php

use yii\db\Migration;

/**
 * Handles adding place to table `material`.
 */
class m180410_120002_add_place_column_to_material_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('material', 'place_eng', $this->string());
        $this->addColumn('material', 'place_esp', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('material', 'place_eng');
        $this->dropColumn('material', 'place_esp');
    }
}
