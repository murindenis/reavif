<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m180405_135917_create_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'address_eng' => $this->string(),
            'address_esp' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'skype' => $this->string(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contacts');
    }
}
