<?php

use yii\db\Migration;

/**
 * Class m180410_083805_add_photo_columns_to
 */
class m180410_083805_add_photo_columns_to extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('material', 'base_url', $this->string());
        $this->addColumn('material', 'path', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('material', 'base_url');
        $this->dropColumn('material', 'path');
    }
}
