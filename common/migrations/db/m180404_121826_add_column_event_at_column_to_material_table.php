<?php

use yii\db\Migration;

/**
 * Handles adding column_event_at to table `material`.
 */
class m180404_121826_add_column_event_at_column_to_material_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('material', 'event_at', $this->integer()->defaultValue(strtotime("now")));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('material', 'event_at');
    }
}
