<?php

use yii\db\Migration;

/**
 * Handles adding apartment to table `shop_order`.
 */
class m180301_081317_add_apartment_column_to_shop_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('shop_order', 'apartment', $this->integer());
        $this->addColumn('shop_order', 'status', $this->boolean()->defaultValue(\common\models\ShopOrder::SHOP_ORDER_NO_PAID));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('shop_order', 'apartment');
        $this->dropColumn('shop_order', 'status');
    }
}
