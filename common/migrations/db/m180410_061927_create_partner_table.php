<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partner`.
 */
class m180410_061927_create_partner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('partner', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'site'       => $this->string(),
            'base_url'   => $this->string(),
            'path'       => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('partner');
    }
}
