<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m180802_161408_create_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('slider', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'path' => $this->string(),
            'base_url' => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('slider');
    }
}
