<?php

use yii\db\Migration;

/**
 * Handles the creation of table `donate`.
 */
class m180411_063732_create_donate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('donate', [
            'id' => $this->primaryKey(),
            'title_eng' => $this->text(),
            'title_esp' => $this->text(),
            'make_donation_eng' => $this->text(),
            'make_donation_esp' => $this->text(),
        ], $tableOptions);

        $this->insert('donate', [
            'title_eng' => 'By donating money to the fund, you help us do more! You can transfer money from a bank card through the MoneyNline system.',
            'title_esp' => 'text esp',
            'make_donation_eng' => 'You can donate through the bank anonymously to 1000 euros. By making donations, you agree to the terms of the offer contract. Download our payment details.',
            'make_donation_esp' => 'text_esp',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('donate');
    }
}
