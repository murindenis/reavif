<?php

use yii\db\Migration;

/**
 * Handles adding site_link to table `partner`.
 */
class m180410_073202_add_site_link_column_to_partner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('partner', 'site_link', $this->string()->after('site'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('partner', 'site_link');
    }
}
