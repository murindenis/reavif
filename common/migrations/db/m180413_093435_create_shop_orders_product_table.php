<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_orders_product`.
 */
class m180413_093435_create_shop_orders_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('shop_orders_product', [
            'id'              => $this->primaryKey(),
            'shop_orders_id'   => $this->integer()->notNull(),
            'shop_product_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-shop_orders_product_shop_orders_id', '{{%shop_orders_product}}', 'shop_orders_id');
        $this->addForeignKey('fk-shop_orders_product_shop_orders_id', '{{%shop_orders_product}}', 'shop_orders_id', '{{%shop_orders}}', 'id', 'cascade', 'cascade');

        $this->createIndex('idx-shop_orders_product_shop_product', '{{%shop_orders_product}}', 'shop_product_id');
        $this->addForeignKey('fk-shop_orders_product_shop_product_id', '{{%shop_orders_product}}', 'shop_product_id', '{{%shop_product}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-shop_orders_product_shop_product_id','shop_orders_product');
        $this->dropIndex('idx-shop_orders_product_shop_product','shop_orders_product');

        $this->dropForeignKey('fk-shop_orders_product_shop_orders_id','shop_orders_product');
        $this->dropIndex('idx-shop_orders_product_shop_order','shop_orders_product');

        $this->dropTable('shop_orders_product');
    }
}
