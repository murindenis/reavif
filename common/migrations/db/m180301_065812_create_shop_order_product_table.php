<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_order_product`.
 */
class m180301_065812_create_shop_order_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('shop_order_product', [
            'id'              => $this->primaryKey(),
            'shop_order_id'   => $this->integer()->notNull(),
            'shop_product_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-shop_order_product_shop_order', '{{%shop_order_product}}', 'shop_order_id');
        $this->addForeignKey('fk-shop_order_product_shop_order_id', '{{%shop_order_product}}', 'shop_order_id', '{{%shop_order}}', 'id', 'cascade', 'cascade');

        $this->createIndex('idx-shop_order_product_shop_product', '{{%shop_order_product}}', 'shop_product_id');
        $this->addForeignKey('fk-shop_order_product_shop_product_id', '{{%shop_order_product}}', 'shop_product_id', '{{%shop_product}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-shop_order_product_shop_product_id','shop_order_product');
        $this->dropIndex('idx-shop_order_product_shop_product','shop_order_product');

        $this->dropForeignKey('fk-shop_order_product_shop_order_id','shop_order_product');
        $this->dropIndex('idx-shop_order_product_shop_order','shop_order_product');

        $this->dropTable('shop_order_product');
    }
}
