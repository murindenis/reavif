<?php

use yii\db\Migration;

/**
 * Handles adding photo to table `shop_product`.
 */
class m180411_082100_add_photo_columns_to_shop_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('shop_product', 'base_url', $this->string());
        $this->addColumn('shop_product', 'path', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('shop_product', 'base_url');
        $this->dropColumn('shop_product', 'path');
    }
}
