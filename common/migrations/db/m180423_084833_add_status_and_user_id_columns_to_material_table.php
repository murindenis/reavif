<?php

use yii\db\Migration;

/**
 * Handles adding status_and_user_id to table `material`.
 */
class m180423_084833_add_status_and_user_id_columns_to_material_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('material', 'status', $this->boolean());
        $this->addColumn('material', 'user_id', $this->integer()->notNull());

        $this->createIndex('idx-material_user_id', '{{%material}}', 'user_id');
        $this->addForeignKey('fk-material_user_id', '{{%material}}', 'user_id', '{{%user}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-material_user_id', 'material');
        $this->dropIndex('idx-material_user_id', 'material');
        $this->dropColumn('material', 'status');
    }
}
