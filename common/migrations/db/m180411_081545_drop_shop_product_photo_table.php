<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `shop_product_photo`.
 */
class m180411_081545_drop_shop_product_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('fk-shop_product_photo_shop_product_id','shop_product_photo');
        $this->dropIndex('idx-shop_product_photo_shop_product','shop_product_photo');
        $this->dropTable('shop_product_photo');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('shop_product_photo', [
            'id' => $this->primaryKey(),
        ]);
        $this->createIndex('idx-shop_product_photo_shop_product', '{{%shop_product_photo}}', 'shop_product_id');
        $this->addForeignKey('fk-shop_product_photo_shop_product_id', '{{%shop_product_photo}}', 'shop_product_id', '{{%shop_product}}', 'id', 'cascade', 'cascade');
    }
}
