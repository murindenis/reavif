<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop_orders`.
 */
class m180412_091543_create_shop_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('shop_orders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-shop_orders_user_id', '{{%shop_orders}}', 'user_id');
        $this->addForeignKey('fk-shop_orders_user_id', '{{%shop_orders}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('shop_orders','fk-shop_orders_user_id');
        $this->dropIndex('shop_orders','idx-shop_orders_user_id');
        $this->dropTable('shop_orders');
    }
}
