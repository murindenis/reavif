<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `material_photo`.
 */
class m180410_083519_drop_material_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('material_photo');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('material_photo', [
            'id' => $this->primaryKey(),
        ]);
    }
}
