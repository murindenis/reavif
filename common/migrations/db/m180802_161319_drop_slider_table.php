<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `slider`.
 */
class m180802_161319_drop_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('slider');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('slider', [
            'id' => $this->primaryKey(),
        ]);
    }
}
