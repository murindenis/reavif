<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "donate".
 *
 * @property int $id
 * @property string $title_eng
 * @property string $title_esp
 * @property string $make_donation_eng
 * @property string $make_donation_esp
 */
class Donate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_eng', 'title_esp', 'make_donation_eng', 'make_donation_esp'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title_eng' => Yii::t('common', 'Title Eng'),
            'title_esp' => Yii::t('common', 'Title Esp'),
            'make_donation_eng' => Yii::t('common', 'Make Donation Eng'),
            'make_donation_esp' => Yii::t('common', 'Make Donation Esp'),
        ];
    }

    public function getTitleLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->title_eng
            : $this->title_esp;
    }

    public function getMakeDonationLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->make_donation_eng
            : $this->make_donation_esp;
    }
}
