<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $address_eng
 * @property string $address_esp
 * @property string $phone
 * @property string $email
 * @property string $skype
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_eng', 'address_esp', 'phone', 'email', 'skype'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'address_eng' => Yii::t('common', 'Address Eng'),
            'address_esp' => Yii::t('common', 'Address Esp'),
            'phone' => Yii::t('common', 'Phone'),
            'email' => Yii::t('common', 'Email'),
            'skype' => Yii::t('common', 'Skype'),
        ];
    }

    public function getAddressLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->address_eng
            : $this->address_esp;
    }
}
