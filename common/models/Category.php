<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name_eng
 * @property string $name_esp
 * @property string $slug
 * @property int $created_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name_eng',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_eng', 'name_esp'], 'required'],
            [['created_at'], 'integer'],
            [['name_eng', 'name_esp', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name_eng' => Yii::t('common', 'Category Name Eng'),
            'name_esp' => Yii::t('common', 'Category Name Esp'),
            'slug' => Yii::t('common', 'Slug'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Material::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHashtags()
    {
        return $this->hasMany(Hashtag::className(), ['hashtag_id' => 'id']);
    }
}
