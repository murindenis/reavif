<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "condition".
 *
 * @property int $id
 * @property string $text_eng
 * @property string $text_esp
 */
class Condition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'condition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_eng', 'text_esp'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'text_eng' => Yii::t('common', 'Text Eng'),
            'text_esp' => Yii::t('common', 'Text Esp'),
        ];
    }

    public function getTextLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->text_eng
            : $this->text_esp;
    }
}
