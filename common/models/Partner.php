<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "partner".
 *
 * @property int $id
 * @property string $name
 * @property string $site
 * @property string $site_link
 * @property string $base_url
 * @property string $path
 * @property int $created_at
 */
class Partner extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at'], 'integer'],
            [['name', 'site', 'site_link', 'base_url', 'path'], 'string', 'max' => 255],
            [['picture'], 'safe'],
            ['site_link', 'url', 'defaultScheme' => 'http']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'site' => Yii::t('common', 'Site Name'),
            'site_link' => Yii::t('common', 'Site link'),
            'base_url' => Yii::t('common', 'Base Url'),
            'path' => Yii::t('common', 'Path'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }
}
