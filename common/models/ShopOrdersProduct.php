<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shop_orders_product".
 *
 * @property int $id
 * @property int $shop_orders_id
 * @property int $shop_product_id
 *
 * @property ShopOrders $shopOrders
 * @property ShopProduct $shopProduct
 */
class ShopOrdersProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_orders_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_orders_id', 'shop_product_id'], 'required'],
            [['shop_orders_id', 'shop_product_id'], 'integer'],
            [['shop_orders_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopOrders::className(), 'targetAttribute' => ['shop_orders_id' => 'id']],
            [['shop_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopProduct::className(), 'targetAttribute' => ['shop_product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'shop_orders_id' => Yii::t('common', 'Shop Orders ID'),
            'shop_product_id' => Yii::t('common', 'Shop Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrders()
    {
        return $this->hasOne(ShopOrders::className(), ['id' => 'shop_orders_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopProduct()
    {
        return $this->hasOne(ShopProduct::className(), ['id' => 'shop_product_id']);
    }
}
