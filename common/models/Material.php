<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\helpers\Url;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "material".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title_eng
 * @property string $title_esp
 * @property string $slug
 * @property int $hashtag_id
 * @property string $introtext_eng
 * @property string $introtext_esp
 * @property string $description_eng
 * @property string $description_esp
 * @property int $created_at
 * @property int $event_at
 * @property string $place_eng
 * @property string $place_esp
 * @property integer $status
 * @property integer $user_id
 *
 * @property Category $category
 * @property Hashtag $hashtag
 */
class Material extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;
    public $hashtags;

    const MATERIAL_ARTICLE = 1;
    const MATERIAL_NEWS    = 2;
    const MATERIAL_EVENTS  = 3;

    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE    = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title_eng',
                'ensureUnique' => true,
                'immutable' => true
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title_eng', 'title_esp', 'slug', 'hashtag_id', 'user_id'], 'required'],
            [['category_id', 'hashtag_id', 'created_at', 'event_at', 'user_id'], 'integer'],
            [['description_eng', 'description_esp', 'place_eng', 'place_esp'], 'string'],
            [['title_eng', 'title_esp', 'slug', 'introtext_eng', 'introtext_esp'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['hashtag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hashtag::className(), 'targetAttribute' => ['hashtag_id' => 'id']],
            [['picture'], 'safe'],
            [['status'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'category_id' => Yii::t('common', 'Category ID'),
            'title_eng' => Yii::t('common', 'Title Eng'),
            'title_esp' => Yii::t('common', 'Title Esp'),
            'slug' => Yii::t('common', 'Slug'),
            'hashtag_id' => Yii::t('common', 'Hashtag ID'),
            'introtext_eng' => Yii::t('common', 'Introtext Eng'),
            'introtext_esp' => Yii::t('common', 'Introtext Esp'),
            'description_eng' => Yii::t('common', 'Description Eng'),
            'description_esp' => Yii::t('common', 'Description Esp'),
            'created_at' => Yii::t('common', 'Created At'),
            'event_at' => Yii::t('common', 'Event At'),
            'place_eng' => Yii::t('common', 'Place Eng'),
            'place_esp' => Yii::t('common', 'Place Esp'),
            'status' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHashtag()
    {
        return $this->hasOne(Hashtag::className(), ['id' => 'hashtag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialHashtag()
    {
        return $this->hasMany(MaterialHashtag::className(), ['material_id' => 'id']);
    }

    public function getHashtagColumn() {
        return Hashtag::find()
            ->select(['name_eng', 'id'])
            ->indexBy('id')
            ->column();
    }
    public function getCategoryColumn() {
        return Category::find()
            ->select(['name_eng', 'id'])
            ->indexBy('id')
            ->column();
    }

    public function getMaterialPhotos()
    {
        return $this->hasMany(MaterialPhoto::className(), ['material_id' => 'id']);
    }

    public function getMaterialIntrotextLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->introtext_eng
            : $this->introtext_esp;
    }

    public function getMaterialDescriptionLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->description_eng
            : $this->description_esp;
    }

    public function getMaterialNameLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->title_eng
            : $this->title_esp;
    }

    public function getMaterialPlaceLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->place_eng
            : $this->place_esp;
    }

    public function getMaterialCategoryNameLang(){
        if (Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG) {
            return (Category::find()->where(['id' => $this->category_id])->one())->name_eng;
        }

        if (Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ESP) {
            return (Category::find()->where(['id' => $this->category_id])->one())->name_esp;
        }
        return false;
    }
    
    public function getMaterialUrl($catId, $slug){
        switch($catId){
            case self::MATERIAL_ARTICLE:
                return Url::toRoute(['/articles/view', 'slug' => $slug]);
                break;
            case self::MATERIAL_NEWS:
                return Url::toRoute(['/news/view', 'slug' => $slug]);
                break;
            case self::MATERIAL_EVENTS:
                return Url::toRoute(['/events/view', 'slug' => $slug]);
                break;
                
        }
        return false;
    }

    public function getMaterialStatus() {
        switch ($this->status){
            case '0':
                return Yii::t('frontend', 'No active');
                break;
            case '1':
                return Yii::t('frontend', 'Active');
                break;
        }
    }
}
