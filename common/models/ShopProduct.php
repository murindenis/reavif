<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "shop_product".
 *
 * @property int $id
 * @property string $name_eng
 * @property string $name_esp
 * @property string $description_eng
 * @property string $description_esp
 * @property int $price
 * @property int $created_at
 * @property string $base_url
 * @property string $path
 */
class ShopProduct extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_product';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_eng', 'name_esp', 'price'], 'required'],
            [['price', 'created_at'], 'integer'],
            [['name_eng', 'name_esp', 'description_eng', 'description_esp', 'base_url', 'path'], 'string', 'max' => 255],
            [['picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name_eng' => Yii::t('common', 'Name Eng'),
            'name_esp' => Yii::t('common', 'Name Esp'),
            'description_eng' => Yii::t('common', 'Description Eng'),
            'description_esp' => Yii::t('common', 'Description Esp'),
            'price' => Yii::t('common', 'Price'),
            'created_at' => Yii::t('common', 'Created At'),
            'base_url' => Yii::t('common', 'Base Url'),
            'path' => Yii::t('common', 'Path'),
        ];
    }

    public function getFirstPhoto()
    {
        return isset($this->productPhotos[0]) ? $this->productPhotos[0]['path'] : '';
    }

    public function getFirstPhotoHref($width, $height, $fit = 'crop')
    {
        return Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $this->getFirstPhoto(),
            'w'    => $width,
            'h'    => $height,
            'fit'  => $fit,
        ], true);
    }

    public function getProductNameLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->name_eng
            : $this->name_esp;
    }
}
