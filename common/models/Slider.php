<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property int $material_id
 * @property int $created_at
 *
 * @property Material $material
 */
class Slider extends \yii\db\ActiveRecord
{
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'integer'],
            [['title', 'path', 'base_url'], 'string', 'max' => 255],
            [['picture'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Text',
            'path' => 'Path',
            'base_url' => 'Base Url',
            'created_at' => 'Created At',
        ];
    }

    public function getArticleColumn() {
        return Material::find()
            ->select(['title_eng', 'id'])
            ->where(['category_id' => Material::MATERIAL_ARTICLE])
            ->indexBy('id')
            ->column();
    }
}
