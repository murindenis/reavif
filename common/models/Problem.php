<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "problem".
 *
 * @property int $id
 * @property string $text_eng
 * @property string $text_esp
 */
class Problem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'problem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_eng', 'text_esp'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_eng' => 'Text Eng',
            'text_esp' => 'Text Esp',
        ];
    }

    public function getProblemTextLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->text_eng
            : $this->text_esp;
    }
}
