<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rekvizit".
 *
 * @property int $id
 * @property string $address_eng
 * @property string $address_esp
 * @property string $ccc
 * @property string $iban_electronic
 * @property string $iban_paper
 * @property string $bic
 * @property string $phone
 */
class Rekvizit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rekvizit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_eng', 'address_esp', 'ccc', 'iban_electronic', 'iban_paper', 'bic', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'address_eng' => Yii::t('common', 'Address Eng'),
            'address_esp' => Yii::t('common', 'Address Esp'),
            'ccc' => Yii::t('common', 'Ccc'),
            'iban_electronic' => Yii::t('common', 'Iban Electronic'),
            'iban_paper' => Yii::t('common', 'Iban Paper'),
            'bic' => Yii::t('common', 'Bic'),
            'phone' => Yii::t('common', 'Phone'),
        ];
    }
    public function getAddressLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->address_eng
            : $this->address_esp;
    }
}
