<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fund".
 *
 * @property int $id
 * @property string $fund_eng
 * @property string $fund_esp
 * @property string $problem_eng
 * @property string $problem_esp
 * @property string $mission_eng
 * @property string $mission_esp
 * @property string $objectives_eng
 * @property string $objectives_esp
 * @property string $partners_eng
 * @property string $partners_esp
 */
class Fund extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fund';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fund_eng', 'problem_eng', 'mission_eng', 'objectives_eng', 'partners_eng'], 'string'],
            [['fund_esp', 'problem_esp', 'mission_esp', 'objectives_esp', 'partners_esp'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'fund_eng' => Yii::t('common', 'Fund REAVIF eng'),
            'fund_esp' => Yii::t('common', 'Fund REAVIF esp'),
            'problem_eng' => Yii::t('common', 'The problem of society eng'),
            'problem_esp' => Yii::t('common', 'The problem of society esp'),
            'mission_eng' => Yii::t('common', 'Mission of organization eng'),
            'mission_esp' => Yii::t('common', 'Mission of organization esp'),
            'objectives_eng' => Yii::t('common', 'Objectives of REAVIF eng'),
            'objectives_esp' => Yii::t('common', 'Objectives of REAVIF esp'),
            'partners_eng' => Yii::t('common', 'Partners of fund eng'),
            'partners_esp' => Yii::t('common', 'Partners of fund esp'),
        ];
    }

    public function getFundLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->fund_eng
            : $this->fund_esp;
    }
    public function getProblemLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->problem_eng
            : $this->problem_esp;
    }
    public function getMissionLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->mission_eng
            : $this->mission_esp;
    }
    public function getObjectivesLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->objectives_eng
            : $this->objectives_esp;
    }
    public function getPartnersLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
            ? $this->partners_eng
            : $this->partners_esp;
    }
}
