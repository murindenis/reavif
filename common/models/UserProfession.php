<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_profession".
 *
 * @property int $id
 * @property string $name_eng
 * @property string $name_esp
 * @property integer $created_at
 */
class UserProfession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profession';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_eng', 'name_esp'], 'required'],
            [['name_eng', 'name_esp'], 'string', 'max' => 255],
            [['created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name_eng' => Yii::t('common', 'Name Eng'),
            'name_esp' => Yii::t('common', 'Name Esp'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }
}
