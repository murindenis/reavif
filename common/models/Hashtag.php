<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "hashtag".
 *
 * @property int $id
 * @property string $name_eng
 * @property string $name_esp
 * @property string $slug
 * @property int $created_at
 */
class Hashtag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hashtag';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name_eng',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_eng', 'name_esp'], 'required'],
            [['created_at'], 'integer'],
            [['name_eng', 'name_esp', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name_eng' => Yii::t('common', 'Hashtag Name Eng'),
            'name_esp' => Yii::t('common', 'Hashtag Name Esp'),
            'slug' => Yii::t('common', 'Slug'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    public function getHashtagNameLang(){
        return Yii::$app->formatter->locale == \common\behaviors\LanguageHelper::LANG_ENG
                    ? $this->name_eng
                    : $this->name_esp;
    }

    public function getMaterialHashtag()
    {
        return $this->hasMany(MaterialHashtag::className(), ['hashtag_id' => 'id']);
    }
}
