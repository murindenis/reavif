<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "material_hashtag".
 *
 * @property int $id
 * @property int $material_id
 * @property int $hashtag_id
 *
 * @property Hashtag $hashtag
 * @property Material $material
 */
class MaterialHashtag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_hashtag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_id', 'hashtag_id'], 'required'],
            [['material_id', 'hashtag_id'], 'integer'],
            [['hashtag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hashtag::className(), 'targetAttribute' => ['hashtag_id' => 'id']],
            [['material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['material_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'material_id' => Yii::t('common', 'Material ID'),
            'hashtag_id' => Yii::t('common', 'Hashtag ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHashtag()
    {
        return $this->hasOne(Hashtag::className(), ['id' => 'hashtag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }
}
